
//=======================================================================
//////////////////////////////////////////////////////////////////////////
// Shaders for the ACTIVE simulation program - based on the phong lighting shader
// in the OpenGL Superbible Version 8
//////////////////////////////////////////////////////////////////////////
//=======================================================================

#version 330 core


// Interpolated values from the vertex shaders
in vec3 Ni;
in vec3 Li;
in vec3 Pi;
flat in vec3 GlyphColor;
flat in vec3 Nf;
flat in vec3 Lf;

// Ouput data
out vec3 finalColor;

// Values that stay constant for the whole mesh.
uniform vec3 LightPosition_worldspace;
uniform vec3 SpecularColor;
uniform vec3 AmbientColorFactor;
uniform vec3 LightColor;
uniform float LightPower;

void main(){

	vec3 N = normalize(Ni);
	vec3 L = normalize(Li);
	vec3 V = -normalize(Pi);
	
	vec3 R = reflect(-L, N);
	
	vec3 diffuse = (max(dot(N, L), 0.0) * 0.85 + max(dot(Nf, Lf), 0.0) * 0.15) * GlyphColor;
	vec3 specular = pow(max(dot(R, V), 0.0), 40) * SpecularColor;
	
	finalColor = AmbientColorFactor * GlyphColor + 
			LightPower * diffuse  + 
			LightPower * specular;

}
