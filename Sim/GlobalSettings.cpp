#include "GlobalSettings.h"
#include <iostream>

Settings s;

int parseSettings(const char* file) {

    SettingsParser settings;
    if(!settings.loadFromFile(file))
    {
        std::cout << "Error loading settings file!" << std::endl;
        return -1;
    }

    settings.get("fragShaderName", s.fragShaderName);
    settings.get("vertShaderName", s.vertShaderName);
    settings.get("projectData", s.projectData);
    settings.get("preCompLocation", s.preCompLocation);
    settings.get("vertexDataFolder", s.vertexDataFolder);
    settings.get("tensorBounds", s.tensorBounds);
    settings.get("ellipseSkipAmount", s.ellipseSkipAmount);
    settings.get("animationStepTimeStart", s.animationStepTimeStart);
    settings.get("lightX", s.lightX);
    settings.get("lightY", s.lightY);
    settings.get("lightZ", s.lightZ);
    settings.get("specularColor", s.specularColor);
    settings.get("ambientColorFactor", s.ambientColorFactor);
    settings.get("lightPowerStart", s.lightPowerStart);
    settings.get("scaleSizeTarget", s.scaleSizeTarget);
    settings.get("lightColorR", s.lightColorR);
    settings.get("lightColorG", s.lightColorG);
    settings.get("lightColorB", s.lightColorB);
    settings.get("meterFactorCoefficient", s.meterFactorCoefficient);
    settings.get("demoMode", s.demoMode);

    return 0;
}
