#ifndef ACTIVE_H_
#define ACTIVE_H_

///////////////////
// Includes
///////////////////

// OGL
#include <GL/gl.h>

// Vrui
#include <Math/Math.h>

#include <Vrui/Vrui.h>
#include <Vrui/Application.h>

#include <Geometry/OrthogonalTransformation.h>

#include <GL/GLShader.h>
#include <GL/GLMaterialTemplates.h>
#include <GL/GLModels.h>
#include <GL/GLColorTemplates.h>
#include <GL/GLVertexTemplates.h>
#include <GL/GLObject.h>
#include <GL/GLContextData.h>
#include <GL/GLGeometryWrappers.h>

#include <GL/Extensions/GLExtension.h>
#include <GL/Extensions/GLARBDrawInstanced.h>
#include <GL/Extensions/GLARBInstancedArrays.h>
#include <GL/Extensions/GLARBVertexBufferObject.h>
#include <GL/Extensions/GLARBVertexProgram.h>
#include <GL/Extensions/GLARBVertexShader.h>
#include <GL/Extensions/GLARBFragmentShader.h>
#include <GL/Extensions/GLARBShaderObjects.h>

#include <GLMotif/Button.h>
#include <GLMotif/Menu.h>
#include <GLMotif/PopupMenu.h>
#include <GLMotif/PopupWindow.h>
#include <GLMotif/ToggleButton.h>
#include <GLMotif/WidgetManager.h>
#include <GLMotif/TextField.h>
#include <GLMotif/Slider.h>
#include <GLMotif/StyleSheet.h>

// GLM
#define GLM_FORCE_RADIANS
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Eigen
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Geometry>

// Standard library
#include <math.h> // use M_PI from here
#include <vector>
#include <fstream>
#include <dirent.h>
#include <map>

// Other
#include "GlobalSettings.h"

using namespace glm;
using namespace std;
using namespace Eigen;

class Active: public Vrui::Application, public GLObject {
public:
	struct DataItem: public GLObject::DataItem // Data structure storing OpenGL-dependent application data
	{
	public:
		// =============================
		// glyph stuff
		// buffer ids
		GLuint vertexbuffer;
		GLuint normalbuffer;
		GLuint elementbuffer;
		// the following three are instance vertex buffers
		GLuint MVbuffer;
		GLuint MVPbuffer;
		GLuint colorbuffer;

		GLShader* shaderaddress;

		GLuint LightPosID;
		GLuint SpecularColorID;
		GLuint AmbientColorFactorID;
		GLuint LightPowerID;
		GLuint LightColorID;

		// These are the matrices and vectors per instance that are sent to the shader
		std::vector<glm::mat4> *MVPs;
		std::vector<glm::mat4> *MVs;

		/* Constructors and destructors: */
		DataItem(void) {
			/* Initialize the GL_ARB_vertex_buffer_object extension: */
			GLARBVertexBufferObject::initExtension();
			// And other extensions for instance rendering
			GLARBDrawInstanced::initExtension();
			GLARBInstancedArrays::initExtension();

			// generate the buffers
			glGenBuffersARB(1, &vertexbuffer);
			glGenBuffersARB(1, &normalbuffer);
			glGenBuffersARB(1, &elementbuffer);
			glGenBuffersARB(1, &MVbuffer);
			glGenBuffersARB(1, &MVPbuffer);
			glGenBuffersARB(1, &colorbuffer);

			MVPs = NULL; // initialized in initBuffers
			MVs = NULL;
			shaderaddress = new GLShader;
		}
		;
		virtual ~DataItem(void) {
			// generate the buffers
			glDeleteBuffersARB(1, &vertexbuffer);
			glDeleteBuffersARB(1, &normalbuffer);
			glDeleteBuffersARB(1, &elementbuffer);
			glDeleteBuffersARB(1, &MVbuffer);
			glDeleteBuffersARB(1, &MVPbuffer);
			glDeleteBuffersARB(1, &colorbuffer);

			delete[] MVPs;
			delete[] MVs;
			delete shaderaddress;
		}
		;
	};

	Active(int& argc, char**& argv); // Initializes the Vrui toolkit and the application
	virtual ~Active(void); // Shuts down the Vrui toolkit

	/* Methods from Vrui::Application: */
	virtual void frame(void); // Called exactly once per frame
	virtual void display(GLContextData& contextData) const; // Called for every eye and every window on every frame

	/* Methods from GLObject: */
	virtual void initContext(GLContextData& contextData) const; // Called once upon creation of each OpenGL context

private:

	////////////////////
	// Members
	////////////////////

	// Data for displaying
	std::vector<std::vector<double*>*> EllipsoidDetails; // 1st index: Animation step, 2nd index: Ellipsoid index, 3nd Index: Ellipsoid data
	std::map<int, std::string> timeStepMap; // maps the 1st index of EllipsoidDetails to the actual timestep (=foldername in the dataset)

	unsigned int* indexGrid; // indices into the vertex buffer, indexed by the alpha and beta values
	unsigned int sizeOfIndexGrid; // = sizeOfEllipsoids*sizeAlpha*sizeBeta
	unsigned int sizeOfEllipsoid; // how many indices are needed to render one ellipsoid - depends on indexing type and levelofdetail
	// Information for indexGrid - these come from the precomputed superellipsoids file
	double minBeta, maxBeta, minAlpha, maxAlpha, parameterStep;
	int levelOfDetail, sizeBeta, sizeAlpha;

	// Animation data
	int curAnimationStep; // 1st index for EllipsoidDetails
	double curAnimationStepTime; // Time elapsed since Animation was last changed
	double animationStepTime; // Time that each step should take
	bool playAnimation;

	// Other stuff important for rendering
	glm::vec4 lightPos;
	float lightPower;
	glm::mat4 datasetTransformation; // computed at startup to make the dataset's center align with the origin of the VRUI coordinate system
	double glyphColorThreshold; // only glyphs with higher alpha values are drawn - controlled by user
	double glyphScalingFactor; // scale glyphs on every axis - controlled by user
	double zAmplificationFactor; // scale glyphs on z axis (which is aligned with the biggest eigenvector) - controlled by user
	bool glyphAlphaScaling; // if false, all glyphs are scaled only in relation to their cell size, if true, they are also scaled by the tensor's alpha value - controlled by user
	bool wireframe;
	bool showLightbulb;

	// These are for storing computation results that are the same across all displays
	std::vector<glm::mat4> *ModelMatrices;
	std::vector<glm::vec3> *Colors;
	bool updateColors;
	bool updateModelMatrices; // these flags are set whenever we need to update the above data, e.g. after an animation step
							  // or when the user changes some options like glyph scaling

	// These are the buffers for the precomputed vertices (they are sent per context to the GPU, but also stored per application here)
	GLfloat *g_vertex_buffer_data;
	GLfloat *g_normal_buffer_data;

	// Stuff related to Vrui Menues
	GLMotif::PopupMenu* mainMenu; // The program's main menu
	GLMotif::PopupWindow* animationDialog; // The animation dialog
	GLMotif::PopupWindow* renderDialog; // guess what :D

	GLMotif::Label* numEllipsoidsLabel;

	GLMotif::Button* resetNavigationButton; // reset the VRUI transform so that the mesh is aligned at the origin

	GLMotif::ToggleButton* showAnimationDialogToggle; // button in the main menu
	GLMotif::ToggleButton* showRenderDialogToggle; // button in the main menu
	GLMotif::ToggleButton* glyphAlphaScalingToggle;
	GLMotif::ToggleButton* wireframeToggle;
	GLMotif::ToggleButton* showLightbulbToggle;
	GLMotif::ToggleButton* playToggle; // Toggle button for automatic playback

	GLMotif::Slider* glyphColorThresholdSlider; // only glyphs with higher alpha value are drawn
	GLMotif::Slider* lightPowerSlider; // influences specular and diffuse color
	GLMotif::Slider* glyphScalingFactorSlider; // stretch the glyphs in all directions
	GLMotif::Slider* zAmplificationSlider; // stretch the glyphs only in the direction of the biggest eigenvector
	GLMotif::Slider* currentTimeSlider; // Slider to adjust the current animation time
	GLMotif::Slider* playSpeedSlider; // Slider to adjust the animation speed

	// the textfields show the sliders' values
	GLMotif::TextField* lightPowerValue;
	GLMotif::TextField* glyphColorThresholdValue;
	GLMotif::TextField* glyphScalingFactorValue;
	GLMotif::TextField* zAmplificationFactorValue;
	GLMotif::TextField* currentTimeValue;
	GLMotif::TextField* playSpeedValue;


	////////////////////
	// Methods
	////////////////////

	// Init methods
	void loadAllSuperEllipsoidDetails();
	bool loadSuperEllipsoidDetails(const char* foldername, int bufferindex); // return: true==time step exists
	void readSuperEllipsoidDataFromFile(const char* Location);
	void loadPrecomputedGlyphs();
	void prepareIndexGrid();
	void bindBuffers(DataItem* dataItem) const;

	// Helper
	void getHeatMapColor(float value, float *red, float *green, float *blue) const;

	// Stuff related to Vrui Menues
	GLMotif::PopupMenu* createMainMenu(void); // Creates the program's main menu
	GLMotif::PopupWindow* createAnimationDialog(void); // Create the animation dialog
	GLMotif::PopupWindow* createRenderDialog(void);

	void toggleButtonCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData);
	void buttonCallback(GLMotif::Button::SelectCallbackData* cbData);
	void sliderCallback(GLMotif::Slider::ValueChangedCallbackData* cbData);
	void menuCloseCallback(GLMotif::PopupWindow::CloseCallbackData* cbData);

	void updateCurrentTimeValue(); // Used by the calback functions and frame()
	void resetNavigation();
};

#endif /* ACTIVE_H_ */
