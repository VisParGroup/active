
//=======================================================================
//////////////////////////////////////////////////////////////////////////
// Shaders for the ACTIVE simulation program - based on the phong lighting shader
// in the OpenGL Superbible Version 8
//////////////////////////////////////////////////////////////////////////
//=======================================================================


#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexNormal_modelspace;
layout(location = 2) in mat4 MV;
layout(location = 6) in mat4 MVP;
layout(location = 10) in vec3 color;

// Output data ; will be interpolated for each fragment.
out vec3 Ni;
out vec3 Li;
out vec3 Pi;
flat out vec3 GlyphColor;
flat out vec3 Nf;
flat out vec3 Lf;

// Values that stay constant for the whole mesh.
uniform vec3 LightPosition_worldspace;
 
void main(){
 
    // Output position of the vertex, in clip space : MVP * position
	gl_Position = MVP * vec4(vertexPosition_modelspace,1);
	
	Pi = (MV * vec4(vertexPosition_modelspace,1)).xyz;
	
	Ni = mat3(MV) * vertexNormal_modelspace;    
	Li = LightPosition_worldspace - Pi;
	GlyphColor = color;
	
	Nf = normalize(Ni);
	Lf = normalize(Li);
}

