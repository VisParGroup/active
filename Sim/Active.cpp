#include "Active.h"

/////////////////////////////
// To Do s:
// TODO test with Kinect
// TODO make glyphs selectable to display additional information
// TODO maybe add ability to move light around?
// TODO performance might be further improved by even fancier drawcalls like glMultiDrawElementsIndirect
//		(then, only one drawcall per display() would be necessary) and by using the gl*Draw*BaseVertex functions
//		the indexgrid could be made much smaller, but all these functions are not yet implemented in VRUI
// TODO some kind of view frustum culling should inrease performance a lot
// TODO different LODs would be cool (dont draw glyphs that will be very small), maybe a tesselation shader
//		could render glyphs of different detail level depending on the size? very fancy but probably difficutl too
/////////////////////////////

Active::Active(int& argc, char**& argv) :
		Vrui::Application(argc, argv), mainMenu(0),
		showAnimationDialogToggle(0), animationDialog(0), currentTimeValue(0),
		currentTimeSlider(0), playSpeedValue(0), playSpeedSlider(0), playToggle(0),
		indexGrid(NULL), ModelMatrices(NULL), Colors(NULL),
		g_vertex_buffer_data(NULL), g_normal_buffer_data(NULL),
		numEllipsoidsLabel(NULL){

	parseSettings("../ActiveSettings.conf");

	lightPos = glm::vec4(s.lightX, s.lightY, s.lightZ, 1);
	playAnimation=true;
	animationStepTime=s.animationStepTimeStart;
	curAnimationStep = 0;
	curAnimationStepTime = 0.0;
	lightPower=s.lightPowerStart;
	glyphColorThreshold = 0.15;
	glyphScalingFactor = 1;
	glyphAlphaScaling = 1;
	wireframe = false;
	showLightbulb = false;
	zAmplificationFactor=1;

	updateColors = true;
	updateModelMatrices = true;

	cout << "Load ellipsoids for every time step" << endl;
	loadAllSuperEllipsoidDetails();

	cout << "Load precomputed info" << endl;
	loadPrecomputedGlyphs();

	cout << "Create indexgrid" << endl;
	sizeOfEllipsoid = // amount of indices per superellipsoids - varies according to the primitive type
			(levelOfDetail-1)*((levelOfDetail)*2+1) // triangle strip (the +1 is for the primitive restart
				;
	sizeOfIndexGrid = (sizeBeta*sizeAlpha)*sizeOfEllipsoid;
	indexGrid = new unsigned int[sizeOfIndexGrid];
	prepareIndexGrid();

	// Create the modelviewmatrix/color buffer per frame
	ModelMatrices = new std::vector<glm::mat4>[(sizeBeta*sizeAlpha)];
	Colors = new std::vector<glm::vec3>[(sizeBeta*sizeAlpha)];

	cout << "Create menues" << endl;
	mainMenu = createMainMenu();
	animationDialog = createAnimationDialog();
	renderDialog = createRenderDialog();

	/* Install the main menu: */
	Vrui::setMainMenu(mainMenu);

	/* Set the navigation transformation: */
	resetNavigation();

	if (s.demoMode) {
		zAmplificationFactor = 2;
		playAnimation = false;
		curAnimationStep = EllipsoidDetails.size() - 1;
		showLightbulb = false;
	}
}

Active::~Active(void) {
	std::cout << "Releasing data and exiting" << endl;
	for (std::vector<std::vector<double*>*>::iterator it =
			EllipsoidDetails.begin(); it != EllipsoidDetails.end(); ++it) {
		for (std::vector<double*>::iterator ot = (*it)->begin();
				ot != (*it)->end(); ++ot) {
			delete [](*ot);
		}
		delete (*it);
	}
	/* Destroy the user interface: */
	delete mainMenu;
	delete animationDialog;
	delete renderDialog;
	delete[] indexGrid;
	delete[] ModelMatrices;
	delete[] Colors;
	delete[] g_vertex_buffer_data;
	delete[] g_normal_buffer_data;
}

void Active::frame(void) {
	/*********************************************************************
	 This function is called exactly once per frame, no matter how many
	 eyes or windows exist. It is the perfect place to change application
	 or Vrui state (run simulations, animate models, synchronize with
	 background threads, change the navigation transformation, etc.).
	 *********************************************************************/

	/* Request another rendering cycle to show the animation: */
	Vrui::scheduleUpdate(Vrui::getApplicationTime() + 1.0 / 125.0); // Aim for 125 FPS

	// We do most of the computations for the frame outside of the display function
	// and also only at the beginning of an animation step or
	// when the user changed some visualization critical option

	// Advance the animation
	if (playAnimation) {
		curAnimationStepTime += Vrui::getFrameTime();
		if (curAnimationStepTime >= animationStepTime) {
			curAnimationStepTime = 0;
			curAnimationStep++;
			if (curAnimationStep >= EllipsoidDetails.size()) {
				curAnimationStep = 0;
			}
			updateCurrentTimeValue();

			// Now we setup all the matrices and stuff that stays constant during this animation step
			updateModelMatrices=true;
			updateColors=true;
		}
	}


	if (updateModelMatrices || updateColors) {
		// Instanced rendering
		// first step: loop through all ellipsoids of this timestep, generate vertex attributes (matrices and color)
		// and sort these attributes according to the precomputed ellipsoid which must be used

		// We store the matrices and vectors per value in the std::vector, which means a lot of slow copy operations
		// but we need them continously in memory to send to the GPU later so i think there is no other way

		for (unsigned int i=0; i<sizeBeta*sizeAlpha; i++) {
			if (updateModelMatrices)
				ModelMatrices[i].clear();
			if (updateColors)
				Colors[i].clear();
		}

		std::vector<double*>* details = EllipsoidDetails.at(
					curAnimationStep);

		unsigned int toDraw = 0;

		for (int ellipseIndex = 0; ellipseIndex < details->size();
				ellipseIndex += 1+s.ellipseSkipAmount) {

			double* ellipse = details->at(ellipseIndex);

			double colorCode = ellipse[12];
			// skip smaller glyphs based on alpha value
			if (colorCode <= glyphColorThreshold)
				continue;

			double alpha = ellipse[0];
			double beta = ellipse[1];

			// index the row
			int indexBeta = round(sizeBeta - 1 + (beta - maxBeta) * (sizeBeta - 1)
									/ (double) (maxBeta - minBeta));
			// index the col
			int indexAlpha = round(sizeAlpha - 1 + (alpha - maxAlpha) * (sizeAlpha - 1)
									/ (double) (maxAlpha - minAlpha));

			// Check for garbage values and don't draw them
			if (indexBeta < 0 || indexAlpha<0)
				continue;

			if (updateModelMatrices) {
				// read the Inputs: Translation, Rotations, scale
				// modify the scale according to the user's options
				double xTrans = ellipse[2];
				double yTrans = ellipse[3];
				double zTrans = ellipse[4];
				double xQuaternion = ellipse[5];
				double yQuaternion = ellipse[6];
				double zQuaternion = ellipse[7];
				double wQuaternion = ellipse[8];
				double xScale = ellipse[9]*glyphScalingFactor*(glyphAlphaScaling?colorCode:1);
				double yScale = ellipse[10]*glyphScalingFactor*(glyphAlphaScaling?colorCode:1);
				double zScale = zAmplificationFactor*ellipse[11]*glyphScalingFactor*(glyphAlphaScaling?colorCode:1);

				// for each Glyph, recompute the modelview matrix.
				glm::mat4 ModelMatrix = // first scale then rotate and then translate then apply transformation for the whole mesh
								datasetTransformation *
								glm::translate(glm::mat4(1.f), glm::vec3((float) xTrans, (float) yTrans, (float) zTrans))
								* glm::toMat4(glm::quat(wQuaternion, xQuaternion, yQuaternion, zQuaternion))
								* glm::scale(glm::mat4(1.f), glm::vec3((float) xScale, (float) yScale, (float) zScale));

				// save the modelmatrix for the display function
				ModelMatrices[indexBeta*sizeAlpha+indexAlpha].push_back(ModelMatrix);
			}

			if (updateColors) {
				// create and save the special glyphcolor
				float r, g, b;
				getHeatMapColor(colorCode, &r, &g, &b);
				Colors[indexBeta*sizeAlpha+indexAlpha].push_back(glm::vec3(r,g,b));
			}

			toDraw++;
		} // end for ellipseindex

		updateColors = false;
		updateModelMatrices = false;


		std::stringstream temp;
		temp << details->size() << " / " << toDraw;
		numEllipsoidsLabel->setString(temp.str().c_str());
	}
}

void Active::display(GLContextData& contextData) const {
	/*********************************************************************
	 This method is called once for every eye in every window on every
	 frame. It must not change application or Vrui state, as it is called
	 an unspecified number of times, and might be called from parallel
	 background threads. It also must not clear the screen or initialize
	 the OpenGL transformation matrices. When this method is called, Vrui
	 will already have rendered its own state (menus etc.) and have set up
	 the transformation matrices so that all rendering in this method
	 happens in navigation (i.e., model) coordinates.
	 *********************************************************************/
	// Here, we draw all our glyphs

	/* Get the OpenGL-dependent application data from the GLContextData object: */
	DataItem* dataItem = contextData.retrieveDataItem<DataItem>(this);

	/* Save OpenGL state: */ // I think we don't need this!
	//glPushAttrib(GL_ENABLE_BIT);
	//glPushAttrib(GL_ALL_ATTRIB_BITS);

	// Enable culling
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

	glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);

	if (wireframe)
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

	// Activate the shader for the glyphs and then deactivate it later so that VRUI can use it's own
	dataItem->shaderaddress->useProgram();

	// We need to retrieve the rendering matrices that VRUI uses
	// These are actually deprecated OpenGL functions - might not work after an update!
	// This is also presumably rather slow
	GLfloat m[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, m);
	glm::mat4 ViewMatrix = glm::make_mat4(m);
	GLfloat p[16];
	glGetFloatv(GL_PROJECTION_MATRIX, p);
	glm::mat4 ProjectionMatrix = glm::make_mat4(p);

	glm::vec4 lightWorld = ViewMatrix * lightPos; // light position in world coordinates

	// pass the light uniforms
	glUniform3fARB(dataItem->LightPosID, lightWorld.x, lightWorld.y, lightWorld.z);
	glUniform3fARB(dataItem->SpecularColorID, s.specularColor, s.specularColor, s.specularColor);
	glUniform3fARB(dataItem->AmbientColorFactorID, s.ambientColorFactor, s.ambientColorFactor, s.ambientColorFactor);
	glUniform3fARB(dataItem->LightColorID, s.lightColorR, s.lightColorG, s.lightColorB);
	glUniform1fARB(dataItem->LightPowerID, lightPower);

	// Instanced rendering
	// Second step: go through all the precomputed ellipsoids that shall be drawn, set up the buffers and draw them through instanced rendering
	for (unsigned int i=0; i<sizeBeta*sizeAlpha; i++) {

		unsigned int NumInstances = ModelMatrices[i].size();
		if (NumInstances <=0)
			continue;

		dataItem->MVPs[i].clear();
		dataItem->MVPs[i].reserve(NumInstances);
		dataItem->MVs[i].clear();
		dataItem->MVs[i].reserve(NumInstances);

		// Now we need to multipliy the precomputed modelviewmatrix with the view and projectionmatrix for this winodw
		for (std::vector<glm::mat4>::const_iterator iter=ModelMatrices[i].begin();
				iter !=ModelMatrices[i].end(); iter++) {
			glm::mat4 MV = (ViewMatrix) * (*iter);
			dataItem->MVs[i].push_back(MV);
			dataItem->MVPs[i].push_back(ProjectionMatrix * MV);
		}

		glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->MVbuffer);
	    glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(glm::mat4) * NumInstances, &((dataItem->MVs[i])[0]), GL_DYNAMIC_DRAW_ARB);

		glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->MVPbuffer);
	    glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(glm::mat4) * NumInstances, &((dataItem->MVPs[i])[0]), GL_DYNAMIC_DRAW_ARB);

		glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->colorbuffer);
	    glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(glm::vec3) * NumInstances, &((Colors[i])[0]), GL_DYNAMIC_DRAW_ARB);

	    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, dataItem->elementbuffer); // necessary?
		glDrawElementsInstancedARB(
			GL_TRIANGLE_STRIP,
			sizeOfEllipsoid,
			GL_UNSIGNED_INT,   // type
			(GLvoid*)(i*sizeOfEllipsoid*sizeof(unsigned int)), // element array buffer offset
			NumInstances
			);
	}

	GLShader::disablePrograms();

	if (wireframe)
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

	glDisable(GL_PRIMITIVE_RESTART_FIXED_INDEX);

	/* Restore OpenGL state: */
	//glPopAttrib();

	if (showLightbulb) {
		/* Draw a sphere for the light */
		glPushMatrix();

		glTranslated(lightPos.x, lightPos.y, lightPos.z);
		glMaterialAmbientAndDiffuse(GLMaterialEnums::FRONT,
				GLColor<GLfloat, 4>(s.lightColorR,s.lightColorG,s.lightColorB));
		glDrawSphereIcosahedron(0.1,2);

		glPopMatrix();
	}
}
/* Create and execute an application object: */
VRUI_APPLICATION_RUN(Active) // this is actually our main() function

///////////////////////
// Init methods
///////////////////////

void Active::initContext(GLContextData& contextData) const {
	/*********************************************************************
	 For classes derived from GLObject, this method is called for each
	 newly-created object, once per every OpenGL context created by Vrui.
	 This method must not change application or Vrui state, but only create
	 OpenGL-dependent application data and store them in the GLContextData
	 object for retrieval in the display method.
	 *********************************************************************/
	// This is called AFTER the ACTIVE constructor!!

	cout << "Init OpenGL context" << endl;

	/* Create context data item and store it in the GLContextData object: */
	DataItem* dataItem = new DataItem;
	contextData.addDataItem(this, dataItem);

// Create and compile our GLSL program from the shaders
	cout << "Loading shaders" << endl;

	dataItem->shaderaddress->initExtensions();
	dataItem->shaderaddress->compileVertexShader(s.vertShaderName.c_str());
	dataItem->shaderaddress->compileFragmentShader(s.fragShaderName.c_str());
	dataItem->shaderaddress->linkShader();
	dataItem->shaderaddress->useProgram();

	cout << "Initializing uniforms" << endl;
	dataItem->LightPosID = dataItem->shaderaddress->getUniformLocation("LightPosition_worldspace");
	dataItem->SpecularColorID = dataItem->shaderaddress->getUniformLocation("SpecularColor");
	dataItem->AmbientColorFactorID = dataItem->shaderaddress->getUniformLocation("AmbientColorFactor");
	dataItem->LightColorID = dataItem->shaderaddress->getUniformLocation("LightColor");
	dataItem->LightPowerID = dataItem->shaderaddress->getUniformLocation("LightPower");

	// Now, send the precomputed glyphs to the gpu buffers
	bindBuffers(dataItem);

	cout << "Init OpenGL context - DONE" << endl;
}

void Active::bindBuffers(DataItem* dataItem) const {

	// These are the buffers for the matrices that are sent to the shader
	// They change every frame, but we initialize them once to avoid memory reallocation
	dataItem->MVPs = new std::vector<glm::mat4>[sizeBeta*sizeAlpha];
	dataItem->MVs = new std::vector<glm::mat4>[sizeBeta*sizeAlpha];

	// now we send the data to the buffers
	// determine the size of the buffers
	unsigned int buffersize = 3 * levelOfDetail * levelOfDetail * sizeAlpha * sizeBeta; // 3D * lod^2 vertices * sizeT times sizeR glyphs

	cout << "Binding vertices to buffers" << endl;
	// VERTEX BUFFER
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->vertexbuffer);
	glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(GLfloat)*buffersize,
			g_vertex_buffer_data, GL_STATIC_DRAW_ARB);

	//NORMALS BUFFER
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->normalbuffer);
	glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(GLfloat)*buffersize,
			g_normal_buffer_data, GL_STATIC_DRAW_ARB);

	cout << "Specify vertexbuffer" << endl;
	glEnableVertexAttribArrayARB(0);
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->vertexbuffer);
	glVertexAttribPointerARB(0, // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*) 0            // array buffer offset
			);

	cout << "Specify normalbuffer" << endl;
	glEnableVertexAttribArrayARB(1);
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->normalbuffer);
	glVertexAttribPointerARB(1,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*) 0                          // array buffer offset
			);

	cout << "Specify elementbuffer" << endl;
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, dataItem->elementbuffer);
	glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, sizeOfIndexGrid * sizeof(unsigned int), &indexGrid[0], GL_STATIC_DRAW_ARB);

	cout << "Prepare MVP, MV and color buffers" << endl;
    // for indexed rendering...
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->MVbuffer);

	for (unsigned int i = 0; i < 4 ; i++) {
		glEnableVertexAttribArrayARB(2 + i);
		glVertexAttribPointerARB(2 + i, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4),
								(const GLvoid*)(sizeof(GLfloat) * i * 4));
		glVertexAttribDivisorARB(2 + i, 1);
	}

	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->MVPbuffer);

	for (unsigned int i = 0; i < 4 ; i++) {
		glEnableVertexAttribArrayARB(6 + i);
		glVertexAttribPointerARB(6 + i, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4),
								(const GLvoid*)(sizeof(GLfloat) * i * 4));
		glVertexAttribDivisorARB(6 + i, 1);
	}

	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->colorbuffer);

	glEnableVertexAttribArrayARB(10);
	glVertexAttribPointerARB(10, 3, GL_FLOAT, GL_FALSE, 0,0);
	glVertexAttribDivisorARB(10, 1);
}


void Active::loadAllSuperEllipsoidDetails() {

	// Here we load the ellipsoids for every timestep

	int bufferindex = 0;

	struct dirent **namelist;
	int n;

	// list the directory contents ...
	n = scandir((s.projectData + s.vertexDataFolder).c_str(), &namelist, 0, versionsort);
	if (n < 0)
		cout << "ERROR: Could not list the superellipsoid files" << endl;
	else {
		for (int i=0; i<n; i++) {
			if (namelist[i]->d_type == DT_DIR) {
				// ... and for each subfolder, check if it contains vertex data generated by the Calculations programm
				if (loadSuperEllipsoidDetails(namelist[i]->d_name, bufferindex))
					bufferindex++;
			}
			free(namelist[i]);
		}
		free(namelist);
	}

	// Load the information to center and scale the whole dataset
	ifstream inputfile;
	inputfile.open((s.projectData + s.tensorBounds).c_str());

	glm::vec3 min, max;
	inputfile >> min.x;
	inputfile >> min.y;
	inputfile >> min.z;
	inputfile >> max.x;
	inputfile >> max.y;
	inputfile >> max.z;

	inputfile.close();

	glm::vec3 toMax = max-min;
	glm::vec3 dataSetCenter = min + (toMax/2.f);
	double dataSetScaleFactor = s.scaleSizeTarget/toMax.length();
	if (dataSetScaleFactor == 0)
		dataSetScaleFactor = 1; // If the setting is 0 in the config, we want no scaling

	std::cout << "Dataset bounds:" << std::endl;
	std::cout << "min " << min.x << " " << min.y << " " << min.z <<  std::endl;
	std::cout << "max " << max.x << " " << max.y << " " << max.z << std::endl;
	std::cout << "center " << dataSetCenter.x << " " << dataSetCenter.y << " " << dataSetCenter.z << " factor " << dataSetScaleFactor << std::endl;

	datasetTransformation = glm::scale(glm::mat4(1.f), glm::vec3(dataSetScaleFactor, dataSetScaleFactor, dataSetScaleFactor))
		* glm::translate(glm::mat4(1.f), glm::vec3(-dataSetCenter.x, -dataSetCenter.y, -dataSetCenter.z));
}

bool Active::loadSuperEllipsoidDetails(const char* foldername, int bufferindex) {
	// specify location
	string VertexDataLocationString = s.projectData + s.vertexDataFolder;
	VertexDataLocationString = VertexDataLocationString + foldername + "/VertexData";

	// check if file exists
    ifstream f(VertexDataLocationString.c_str());
    if (f.good()) {
        f.close();

        // the file exists so now load all information for that timestep
        EllipsoidDetails.push_back(new std::vector<double*>);
    	cout << "Loading ellipsoids of time step " << foldername << ", ";
    	readSuperEllipsoidDataFromFile(VertexDataLocationString.c_str());
    	cout << "number of ellipsoids = " << EllipsoidDetails.back()->size() << endl;

    	// here we store the name of the timestep that belongs to the index in the EllipsoidDetails buffer
    	timeStepMap[bufferindex]=foldername;
    	return true;
    } else {
        f.close();
        return false;
    }
}

void Active::readSuperEllipsoidDataFromFile(const char* Location) {

	// read! not draw!
	// for a single vertex data file, load the superellipoids

	string VertexDataLocationString = Location;
	VertexDataLocationString.append("Info");
	const char* VertexDataInfoLocation = VertexDataLocationString.c_str();
	ifstream inputfile;
	inputfile.open(VertexDataInfoLocation);

	// this file contains only the total number of all ellipsoids
	int totalNumber;
	inputfile >> totalNumber;
	// close file
	inputfile.close();

	// resize storage
	EllipsoidDetails.back()->resize(totalNumber, NULL); // the last std::vector is the one we want to fill now
														// fill each element with NULL - we create the double array
														// for each glyph in the loop

	inputfile.open(Location, ios::in | ios::binary);

	// now for each superellipsoid
	for (int indexOfEllipse = 0; indexOfEllipse < totalNumber;
			indexOfEllipse++) {
		// read the data for a single ellipsoid
		double *p = new double[13];
		EllipsoidDetails.back()->at(indexOfEllipse) = p;
		inputfile.read( (char*)p, sizeof(double)*13);
	} // end indexOfEllipse for

	inputfile.close();
}

void Active::loadPrecomputedGlyphs() {

	cout << "Load the precomputed superellipsoids" << endl;

	// here read the precomputed superellipsoids info file
	// because the prepareIndexGrid function needs the information here
	// we also create the application buffer, but we do not bind to any GPU buffers (that happens in bindBuffer())

	string PreComputedSuperEllipsoidsFileLocation = s.projectData + s.preCompLocation;
	string info = PreComputedSuperEllipsoidsFileLocation + "Info";

	//open file
	ifstream preComputedStream;
	preComputedStream.open(info.c_str());

	//read first lines
	string line;
	// store some settings for later use
	preComputedStream >> line >> minBeta;
	preComputedStream >> line >> maxBeta;
	preComputedStream >> line >> minAlpha;
	preComputedStream >> line >> maxAlpha;
	preComputedStream >> line >> parameterStep;
	preComputedStream >> line >> levelOfDetail;

	// compute total size of the glyph grid
	sizeBeta = round((maxBeta - minBeta) / parameterStep);
	sizeAlpha = round((maxAlpha - minAlpha) / parameterStep);

	std::cout << "Index grid size: " << sizeBeta * sizeAlpha << std::endl;

	//close file
	preComputedStream.close();

	unsigned int buffersize = 3 * levelOfDetail * levelOfDetail * sizeAlpha * sizeBeta; // 3D * lod^2 vertices * sizeT times sizeR glyphs
	cout << "buffersize is = " << buffersize << endl;

	g_vertex_buffer_data = new GLfloat[buffersize];
	g_normal_buffer_data = new GLfloat[buffersize];

	ifstream InFile;
	string vert = PreComputedSuperEllipsoidsFileLocation + "Vert";
	InFile.open(vert.c_str(), ios::in | ios::binary);
	InFile.read( (char*)g_vertex_buffer_data, sizeof(unsigned int)*buffersize);
	InFile.close();
	string norm = PreComputedSuperEllipsoidsFileLocation + "Norm";
	InFile.open(norm.c_str(), ios::in | ios::binary);
	InFile.read( (char*)g_normal_buffer_data, sizeof(unsigned int)*buffersize);
	InFile.close();
}

void Active::prepareIndexGrid() {
	// we run this method once to prepare our set of indices pointing
	// into the vertex buffer that is set up in the "bindBuffers" function
	// later, we will use only this index buffer to do the drawing

	// compute sizes of the indexing
	int sizeOfBetaIndexBuffer = levelOfDetail * levelOfDetail * sizeAlpha;
	int sizeOfAlphaIndexbuffer = levelOfDetail * levelOfDetail;

	// loop through parameters, and create the necessary indexing
	for (int indexBeta = 0; indexBeta < sizeBeta; indexBeta++) {

		int offsetBeta = indexBeta * sizeOfBetaIndexBuffer;

		for (int indexAlpha = 0; indexAlpha < sizeAlpha; indexAlpha++) {

			int offsetAlpha = indexAlpha * sizeOfAlphaIndexbuffer;

			int indexLOD = 0;

			// loop through the vertices
			for (int j = 0; j < (levelOfDetail-1); j++) {

				for (int i = 0; i < (levelOfDetail); i++) {
				
					int indexOffset;

					// TIANGLE_STRIP style - less indices
					indexOffset = offsetBeta + offsetAlpha + (j + 1) * levelOfDetail + (i + 0);
					indexGrid[(indexBeta*sizeAlpha+indexAlpha)*sizeOfEllipsoid+indexLOD] = indexOffset;
					indexLOD++;

					indexOffset = offsetBeta + offsetAlpha + (j + 0) * levelOfDetail + (i + 0);
					indexGrid[(indexBeta*sizeAlpha+indexAlpha)*sizeOfEllipsoid+indexLOD] = indexOffset;
					indexLOD++;
				}

				// we need to restart the triangle strip each time the inner loop is completed
				// without this, the ellipsoid will have weird triangles on one edge
				// see also http://paulbourke.net/geometry/superellipse/
				indexGrid[(indexBeta*sizeAlpha+indexAlpha)*sizeOfEllipsoid+indexLOD] = 0xFFFFFFFF;
				indexLOD++;

			} // end forloop vertices
		}
	} // end for grid
}

// from http://www.andrewnoske.com/wiki/Code_-_heatmaps_and_color_gradients
void Active::getHeatMapColor(float value, float *red, float *green, float *blue) const
{
	// This maps a float value between 0 and 1 to the visible color spectrum (from blue over green to red)

  const int NUM_COLORS = 4;
  static float color[NUM_COLORS][3] = { {0,0,1}, {0,1,0}, {1,1,0}, {1,0,0} };
	// A static array of 4 colors:  (blue,   green,  yellow,  red) using {r,g,b} for each.

  int idx1;        // |-- Our desired color will be between these two indexes in "color".
  int idx2;        // |
  float fractBetween = 0;  // Fraction between "idx1" and "idx2" where our value is.

  if(value <= 0)      {  idx1 = idx2 = 0;            }    // accounts for an input <=0
  else if(value >= 1)  {  idx1 = idx2 = NUM_COLORS-1; }    // accounts for an input >=0
  else
  {
	value = value * (NUM_COLORS-1);        // Will multiply value by 3.
	idx1  = floor(value);                  // Our desired color will be after this index.
	idx2  = idx1+1;                        // ... and before this index (inclusive).
	fractBetween = value - float(idx1);    // Distance between the two indexes (0-1).
  }

  *red   = (color[idx2][0] - color[idx1][0])*fractBetween + color[idx1][0];
  *green = (color[idx2][1] - color[idx1][1])*fractBetween + color[idx1][1];
  *blue  = (color[idx2][2] - color[idx1][2])*fractBetween + color[idx1][2];
}

///////////////////////
// Stuff related to Vrui Menues
///////////////////////

GLMotif::PopupMenu* Active::createMainMenu(void) {
	/* Create a popup shell to hold the main menu: */
	GLMotif::PopupMenu* mainMenuPopup = new GLMotif::PopupMenu("MainMenuPopup",
			Vrui::getWidgetManager());
	mainMenuPopup->setTitle("A.C.T.I.V.E");

	/* Create the main menu itself: */
	GLMotif::Menu* mainMenu = new GLMotif::Menu("MainMenu", mainMenuPopup,
			false);

	/* Create a button: */
	resetNavigationButton = new GLMotif::Button(
			"ResetNavigationButton", mainMenu, "Reset Navigation");

	/* Add a callback to the button: */
	resetNavigationButton->getSelectCallbacks().add(this,
			&Active::buttonCallback);

	/* Create a toggle button to show the animation dialog: */
	showAnimationDialogToggle = new GLMotif::ToggleButton(
			"ShowAnimationDialogToggle", mainMenu, "Show Animation Dialog");
	showAnimationDialogToggle->setToggle(false);
	showAnimationDialogToggle->getValueChangedCallbacks().add(this,
			&Active::toggleButtonCallback);

	showRenderDialogToggle = new GLMotif::ToggleButton(
			"ShowLightDialogToggle", mainMenu, "Show Render Settings");
	showRenderDialogToggle->setToggle(false);
	showRenderDialogToggle->getValueChangedCallbacks().add(this,
			&Active::toggleButtonCallback);

	/* Finish building the main menu: */
	mainMenu->manageChild();

	return mainMenuPopup;
}

GLMotif::PopupWindow* Active::createAnimationDialog(void) {
	const GLMotif::StyleSheet& ss = *Vrui::getWidgetManager()->getStyleSheet();

	GLMotif::PopupWindow* animationDialogPopup = new GLMotif::PopupWindow(
			"AnimationDialogPopup", Vrui::getWidgetManager(), "Animation");
	animationDialogPopup->setResizableFlags(true, false);
	animationDialogPopup->setCloseButton(true);
	animationDialogPopup->getCloseCallbacks().add(this,
			&Active::menuCloseCallback);

	GLMotif::RowColumn* animationDialog = new GLMotif::RowColumn(
			"AnimationDialog", animationDialogPopup, false);
	animationDialog->setNumMinorWidgets(3);

	new GLMotif::Label("CurrentTimeLabel", animationDialog, "Current step");

	currentTimeValue = new GLMotif::TextField("CurrentTimeValue",
			animationDialog, 19);
	updateCurrentTimeValue();

	currentTimeSlider = new GLMotif::Slider("CurrentTimeSlider",
			animationDialog, GLMotif::Slider::HORIZONTAL,
			ss.fontHeight * 15.0f);
	currentTimeSlider->setValueRange(0, EllipsoidDetails.size()-1, 1);
	currentTimeSlider->setValue(curAnimationStep);
	currentTimeSlider->getValueChangedCallbacks().add(this,
			&Active::sliderCallback);

	new GLMotif::Label("PlaySpeedLabel", animationDialog, "Time per step");

	playSpeedValue = new GLMotif::TextField("PlaySpeedValue", animationDialog,
			6);
	playSpeedValue->setFieldWidth(6);
	playSpeedValue->setPrecision(3);
	playSpeedValue->setValue(animationStepTime);

	playSpeedSlider = new GLMotif::Slider("PlaySpeedSlider", animationDialog,
			GLMotif::Slider::HORIZONTAL, ss.fontHeight * 10.0f);
	playSpeedSlider->setValueRange(0.01, 1.0, 0.01);
	playSpeedSlider->setValue(animationStepTime);
	playSpeedSlider->getValueChangedCallbacks().add(this,
			&Active::sliderCallback);

	playToggle = new GLMotif::ToggleButton("PlayToggle", animationDialog,
			"Toggle animation");
	playToggle->setToggle(playAnimation);
	playToggle->getValueChangedCallbacks().add(this,
			&Active::toggleButtonCallback);

	new GLMotif::Label("NumEllispoidsLabelInfo", animationDialog, "Ellipsoids (count/drawn):");
	numEllipsoidsLabel = new GLMotif::Label("NumEllispoidsLabel", animationDialog, "");

	animationDialog->manageChild();

	return animationDialogPopup;
}

GLMotif::PopupWindow* Active::createRenderDialog(void) {
	const GLMotif::StyleSheet& ss = *Vrui::getWidgetManager()->getStyleSheet();

	GLMotif::PopupWindow* lightDialogPopup = new GLMotif::PopupWindow(
			"LightDialogPopup", Vrui::getWidgetManager(), "Render settings");
	lightDialogPopup->setResizableFlags(true, false);
	lightDialogPopup->setCloseButton(true);
	lightDialogPopup->getCloseCallbacks().add(this,
			&Active::menuCloseCallback);

	GLMotif::RowColumn* lightDialog = new GLMotif::RowColumn(
			"LightDialog", lightDialogPopup, false);
	lightDialog->setNumMinorWidgets(3);

	new GLMotif::Label("LightPowerLabel", lightDialog, "Light power");

	lightPowerValue = new GLMotif::TextField("LightPowerValue",
			lightDialog, 19);
	lightPowerValue->setValue(lightPower);

	lightPowerSlider = new GLMotif::Slider("LightPowerSlider",
			lightDialog, GLMotif::Slider::HORIZONTAL,
			ss.fontHeight * 15.0f);
	lightPowerSlider->setValueRange(0,20,0.1);
	lightPowerSlider->setValue(lightPower);
	lightPowerSlider->getValueChangedCallbacks().add(this,
			&Active::sliderCallback);

	new GLMotif::Label("glyphColorThresholdL", lightDialog, "Alpha threshold");

	glyphColorThresholdValue = new GLMotif::TextField("glyphColorThresholdValue",
			lightDialog, 19);
	glyphColorThresholdValue->setValue(glyphColorThreshold);

	glyphColorThresholdSlider = new GLMotif::Slider("glyphColorThresholdSlider",
			lightDialog, GLMotif::Slider::HORIZONTAL,
			ss.fontHeight * 15.0f);
	glyphColorThresholdSlider->setValueRange(0,1,0.01);
	glyphColorThresholdSlider->setValue(glyphColorThreshold);
	glyphColorThresholdSlider->getValueChangedCallbacks().add(this,
			&Active::sliderCallback);

	new GLMotif::Label("glyphScalingFactorL", lightDialog, "Glyphs scaling");

	glyphScalingFactorValue = new GLMotif::TextField("glyphScalingFactorValue",
			lightDialog, 19);
	glyphScalingFactorValue->setValue(glyphScalingFactor);

	glyphScalingFactorSlider = new GLMotif::Slider("glyphScalingFactorSlider",
			lightDialog, GLMotif::Slider::HORIZONTAL,
			ss.fontHeight * 15.0f);
	glyphScalingFactorSlider->setValueRange(0.01,2,0.01);
	glyphScalingFactorSlider->setValue(glyphScalingFactor);
	glyphScalingFactorSlider->getValueChangedCallbacks().add(this,
			&Active::sliderCallback);

	new GLMotif::Label("zAmplificationFactorValueL", lightDialog, "z-Axis amplification");

	zAmplificationFactorValue = new GLMotif::TextField("zAmplificationFactorValue",
			lightDialog, 19);
	zAmplificationFactorValue->setValue(zAmplificationFactor);

	zAmplificationSlider = new GLMotif::Slider("zAmplificationSlider",
			lightDialog, GLMotif::Slider::HORIZONTAL,
			ss.fontHeight * 15.0f);
	zAmplificationSlider->setValueRange(1,3,0.01);
	zAmplificationSlider->setValue(zAmplificationFactor);
	zAmplificationSlider->getValueChangedCallbacks().add(this,
			&Active::sliderCallback);

	glyphAlphaScalingToggle = new GLMotif::ToggleButton("glyphAlphaScalingT", lightDialog,
				"Toggle glyph scaling by alpha value");
	glyphAlphaScalingToggle->setToggle(glyphAlphaScaling);
	glyphAlphaScalingToggle->getValueChangedCallbacks().add(this,
			&Active::toggleButtonCallback);

	wireframeToggle = new GLMotif::ToggleButton("wireframeToggle", lightDialog,
			"Toggle wireframe");
	wireframeToggle->setToggle(wireframe);
	wireframeToggle->getValueChangedCallbacks().add(this,
			&Active::toggleButtonCallback);

	showLightbulbToggle = new GLMotif::ToggleButton("showLightbulbToggle", lightDialog,
			"Show lightbulb");
	showLightbulbToggle->setToggle(showLightbulb);
	showLightbulbToggle->getValueChangedCallbacks().add(this,
			&Active::toggleButtonCallback);

	lightDialog->manageChild();

	return lightDialogPopup;
}

void Active::updateCurrentTimeValue() {
	if (currentTimeValue!=NULL)
	currentTimeValue->setString(timeStepMap[curAnimationStep].c_str());
	if (currentTimeSlider!=NULL)
	currentTimeSlider->setValue(curAnimationStep);
}

void Active::resetNavigation() {
	/* Set the navigation transformation: */
	Vrui::NavTransform t = Vrui::NavTransform::identity;
	t *= Vrui::NavTransform::translateFromOriginTo(Vrui::getDisplayCenter());
	t *= Vrui::NavTransform::scale(s.meterFactorCoefficient * Vrui::getMeterFactor()); // we assume our nav space unit is one meter * meterFactorCoefficient
	Vrui::setNavigationTransformation(t);
}

void Active::toggleButtonCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData){
	if (cbData->toggle == playToggle) {
		playAnimation=cbData->set;
	} else if (cbData->toggle == wireframeToggle) {
		wireframe=cbData->set;
	} else if (cbData->toggle == showLightbulbToggle) {
		showLightbulb=cbData->set;
	} else if (cbData->toggle == glyphAlphaScalingToggle) {
		glyphAlphaScaling=cbData->set;
		updateModelMatrices=true;
	} else if (cbData->toggle == showRenderDialogToggle) {
		if (cbData->set) {
			Vrui::popupPrimaryWidget(renderDialog);
		} else {
			Vrui::popdownPrimaryWidget(renderDialog);
		}
	} else if (cbData->toggle == showAnimationDialogToggle) {
		if (cbData->set) {
			Vrui::popupPrimaryWidget(animationDialog);
		} else {
			Vrui::popdownPrimaryWidget(animationDialog);
		}
	}
}

void Active::buttonCallback(GLMotif::Button::SelectCallbackData* cbData) {
	if (cbData->button == resetNavigationButton) {
		/* Reset the Vrui navigation transformation: */
		resetNavigation();
	}
}

void Active::sliderCallback(GLMotif::Slider::ValueChangedCallbackData* cbData){
	if (cbData->slider == playSpeedSlider) {
		animationStepTime=cbData->value;
		playSpeedValue->setValue(animationStepTime);
	} else if (cbData->slider == glyphColorThresholdSlider) {
		glyphColorThreshold=cbData->value;
		glyphColorThresholdValue->setValue(glyphColorThreshold);
		updateModelMatrices=true;
		updateColors=true;
	} else if (cbData->slider == lightPowerSlider) {
		lightPower=cbData->value;
		lightPowerValue->setValue(lightPower);
	} else if (cbData->slider == glyphScalingFactorSlider) {
		glyphScalingFactor=cbData->value;
		glyphScalingFactorValue->setValue(glyphScalingFactor);
		updateModelMatrices=true;
	} else if (cbData->slider == zAmplificationSlider) {
		zAmplificationFactor=cbData->value;
		zAmplificationFactorValue->setValue(zAmplificationFactor);
		updateModelMatrices=true;
	} else if (cbData->slider == currentTimeSlider) {
		curAnimationStep=cbData->value;
		curAnimationStepTime=0;
		updateCurrentTimeValue();
		updateModelMatrices=true;
		updateColors=true;
	}
}

void Active::menuCloseCallback(GLMotif::PopupWindow::CloseCallbackData* cbData){
	if (cbData->popupWindow == animationDialog) {
		showAnimationDialogToggle->setToggle(false);
	} else if (cbData->popupWindow == renderDialog) {
		showRenderDialogToggle->setToggle(false);
	}
}
