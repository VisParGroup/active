#ifndef GLOBALSETTINGS_H_
#define GLOBALSETTINGS_H_

#include "SettingsParser.hpp"

//=======================================================================
//////////////////////////////////////////////////////////////////////////
// Global settings for the program
//////////////////////////////////////////////////////////////////////////
//=======================================================================

struct Settings {
	std::string fragShaderName = "ActiveFragmentShader.glsl";
	std::string vertShaderName = "ActiveVertexShader.glsl";

	std::string projectData = "/home/internshipdude/Desktop/projectdata/";
	std::string preCompLocation = "constant/PreComputedSuperEllipsoids";
	std::string vertexDataFolder = "concretePourLong/";
	std::string tensorBounds = "constant/polyMesh/meshbounds";

	int ellipseSkipAmount = 0; // Higher values mean less glyphs are drawn

	// Some rendering options, most can be changed by the user in the application
	float animationStepTimeStart = 0.05; // in seconds
	float lightX = 0;// light position
	float lightY = 0;
	float lightZ = 0;
	float specularColor = 0.3;
	float ambientColorFactor = 0.3;// multiplied with the glyph color for ambient lighting
	float lightPowerStart = 1;
	float scaleSizeTarget = 10;
	float lightColorR = 1;
	float lightColorG = 1;
	float lightColorB = 1;

	float meterFactorCoefficient = 1;

	bool demoMode = false;
};

extern Settings s;

int parseSettings(const char* file);

#endif /* GLOBALSETTINGS_H_ */
