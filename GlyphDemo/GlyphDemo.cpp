#include "GlyphDemo.h"

GlyphDemo::GlyphDemo(int& argc, char**& argv) :
		Vrui::Application(argc, argv), mainMenu(0),
		indexGrid(NULL), 
		g_vertex_buffer_data(NULL), g_normal_buffer_data(NULL){

	parseSettings("../ActiveSettings.conf");

	lightPos = glm::vec4(s.lightX, s.lightY, s.lightZ, 1);
	lightPower=s.lightPowerStart;
	wireframe = false;
	fill = true;
	coordSystem = true;
	tensorTest = false;
	glyphScalingFactor = 1;
	zAmplificationFactor = 1;
	
	tensor <<
				0.5, 0, 0,
				0, 0.5, 0,
				0, 0, 0;

	cout << "Load precomputed info" << endl;
	loadPrecomputedGlyphs();

	cout << "Create indexgrid" << endl;
	sizeOfEllipsoid = // amount of indices per superellipsoids - varies according to the primitive type
			(levelOfDetail-1)*((levelOfDetail)*2+1) // triangle strip (the +1 is for the primitive restart
				;
	sizeOfIndexGrid = (sizeBeta*sizeAlpha)*sizeOfEllipsoid;
	indexGrid = new unsigned int[sizeOfIndexGrid];
	prepareIndexGrid();

	cout << "Create menues" << endl;
	mainMenu = createMainMenu();
	renderDialog = createRenderDialog();
	alpha = minAlpha;
	beta = minBeta;
	glyphDemoDialog = createGlyphDemoDialog();
	
	/* Install the main menu: */
	Vrui::setMainMenu(mainMenu);

	/* Set the navigation transformation: */
	resetNavigation();
	reloadTensor();

}

GlyphDemo::~GlyphDemo(void) {
	std::cout << "Releasing data and exiting" << endl;
	/* Destroy the user interface: */
	delete mainMenu;
	delete renderDialog;
	delete[] indexGrid;
	delete[] g_vertex_buffer_data;
	delete[] g_normal_buffer_data;
	delete glyphDemoDialog;
}

void GlyphDemo::frame(void) {
	/*********************************************************************
	 This function is called exactly once per frame, no matter how many
	 eyes or windows exist. It is the perfect place to change application
	 or Vrui state (run simulations, animate models, synchronize with
	 background threads, change the navigation transformation, etc.).
	 *********************************************************************/

	/* Request another rendering cycle to show the animation: */
	Vrui::scheduleUpdate(Vrui::getApplicationTime() + 1.0 / 125.0); // Aim for 125 FPS

}

void GlyphDemo::display(GLContextData& contextData) const {
	//cout << "display function start!" << endl;
	/*********************************************************************
	 This method is called once for every eye in every window on every
	 frame. It must not change application or Vrui state, as it is called
	 an unspecified number of times, and might be called from parallel
	 background threads. It also must not clear the screen or initialize
	 the OpenGL transformation matrices. When this method is called, Vrui
	 will already have rendered its own state (menus etc.) and have set up
	 the transformation matrices so that all rendering in this method
	 happens in navigation (i.e., model) coordinates.
	 *********************************************************************/

	/* Get the OpenGL-dependent application data from the GLContextData object: */
	DataItem* dataItem = contextData.retrieveDataItem<DataItem>(this);

	dataItem->shaderaddress->useProgram();
		
	glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);

	GLfloat m[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, m);
	glm::mat4 ViewMatrix = glm::make_mat4(m);
	GLfloat p[16];
	glGetFloatv(GL_PROJECTION_MATRIX, p);
	glm::mat4 ProjectionMatrix = glm::make_mat4(p);

	glm::vec4 lightWorld = ViewMatrix * glm::vec4(lightPos.x,lightPos.y,lightPos.z,1);

	// pass the light uniform
	glUniform3fARB(dataItem->LightPosID, lightWorld.x, lightWorld.y, lightWorld.z);
	glUniform3fARB(dataItem->SpecularColorID, s.specularColor, s.specularColor, s.specularColor);
	glUniform3fARB(dataItem->AmbientColorFactorID, s.ambientColorFactor, s.ambientColorFactor, s.ambientColorFactor);
	glUniform3fARB(dataItem->LightColorID, s.lightColorR, s.lightColorG, s.lightColorB);
	glUniform1fARB(dataItem->LightPowerID, lightPower);

	double xQuaternion, yQuaternion, zQuaternion, wQuaternion,
		 xScale, yScale, zScale;
	double alphaTemp = alpha;
	double betaTemp = beta;
		
	if (tensorTest) {
		VectorXd tensorIntel=eigenValuesToSuperEllipsoidParameters(tensor, 3, 3);
		alphaTemp = tensorIntel(0);
		betaTemp = tensorIntel(1);

		if(betaTemp > maxBeta) betaTemp=maxBeta;
		if(alphaTemp > maxAlpha) alphaTemp=maxAlpha;
		if(betaTemp < minBeta) betaTemp=minBeta;
		if(alphaTemp < minAlpha) alphaTemp=minAlpha;

		// read the Inputs: x,y,z Translation, x,y,z Rotations, scale,
		xQuaternion = tensorIntel(2);
		yQuaternion = tensorIntel(3);
		zQuaternion = tensorIntel(4);
		wQuaternion = tensorIntel(5);
		xScale = tensorIntel(6);
		yScale = tensorIntel(7);
		zScale = tensorIntel(8);
	} else {
		// Identity quaternion
		xQuaternion = 0;
		yQuaternion = 0;
		zQuaternion = 0;
		wQuaternion = 1;
		
		xScale = 1;
		yScale = 1;
		zScale = 1;
	}

	// index the row
	int indexBeta = round(sizeBeta - 1 + (betaTemp - maxBeta) * (sizeBeta - 1)
							/ (double) (maxBeta - minBeta));
	// index the col
	int indexAlpha = round(sizeAlpha - 1 + (alphaTemp - maxAlpha) * (sizeAlpha - 1)
							/ (double) (maxAlpha - minAlpha));
	// for each Glyph, recompute the modelview matrix.
	// Model matrix, and send it directly to the shader
	glm::mat4 ModelMatrix = // first scale then rotate and then translate then apply transformation for the whole mesh
					// glm::translate(glm::mat4(1.f), glm::vec3((float) xTrans, (float) yTrans, (float) zTrans))
					glm::toMat4(glm::quat(wQuaternion, xQuaternion, yQuaternion, zQuaternion))
					* glm::scale(glm::mat4(1.f), glm::vec3((float) xScale, (float) yScale, (float) zScale));

	// create and send the special glyphcolor
	float r=1;
	float g=0.75;
	float b=0.5;

	unsigned int NumInstances = 1;

	dataItem->MVPs[0].clear();
	dataItem->MVPs[0].reserve(NumInstances);
	dataItem->MVs[0].clear();
	dataItem->MVs[0].reserve(NumInstances);
	std::vector<glm::vec3> Color;
	Color.push_back(glm::vec3(r,g,b));

	glm::mat4 MV = (ViewMatrix) * ModelMatrix;
	dataItem->MVs[0].push_back(MV);
	dataItem->MVPs[0].push_back(ProjectionMatrix * MV);

	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->MVbuffer);
    	glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(glm::mat4) * NumInstances, &((dataItem->MVs[0])[0]), GL_DYNAMIC_DRAW_ARB);

	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->MVPbuffer);
    	glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(glm::mat4) * NumInstances, &((dataItem->MVPs[0])[0]), GL_DYNAMIC_DRAW_ARB);

	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->colorbuffer);
    	glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(glm::vec3) * NumInstances, &(Color[0]), GL_DYNAMIC_DRAW_ARB);

    	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, dataItem->elementbuffer); // necessary?
    	
    	if (fill) {		
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
		glDrawElementsInstancedARB(
			GL_TRIANGLE_STRIP,
			sizeOfEllipsoid,
			GL_UNSIGNED_INT,   // type
			(GLvoid*)((indexBeta*sizeAlpha+indexAlpha)*sizeOfEllipsoid*sizeof(unsigned int)), // element array buffer offset
			NumInstances
			);
	}
	if (wireframe) {	
		// Wireframe gets another color
	    	Color.clear();
		Color.push_back(glm::vec3(0.3,0.3,0.3));
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->colorbuffer);
	    	glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(glm::vec3) * NumInstances, &(Color[0]), GL_DYNAMIC_DRAW_ARB);
	    	
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
		glDrawElementsInstancedARB(
			GL_TRIANGLE_STRIP,
			sizeOfEllipsoid,
			GL_UNSIGNED_INT,   // type
			(GLvoid*)((indexBeta*sizeAlpha+indexAlpha)*sizeOfEllipsoid*sizeof(unsigned int)), // element array buffer offset
			NumInstances
			);
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
	}
	
	GLShader::disablePrograms();

	glDisable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
	
	if (coordSystem) {
		/* Draw a coordinate system */
		glPushMatrix();

		// x Axis red
		glRotated(90,0,1,0);
		glMaterialAmbientAndDiffuse(GLMaterialEnums::FRONT,
				GLColor<GLfloat, 4>(1,0,0));
		glDrawArrow(0.05,0.15,0.3,5,30);

		glPopMatrix();
		glPushMatrix();

		// y Axis green
		glRotated(90,1,0,0);
		glMaterialAmbientAndDiffuse(GLMaterialEnums::FRONT,
				GLColor<GLfloat, 4>(0,1,0));
		glDrawArrow(0.05,0.15,0.3,5,30);

		glPopMatrix();
		glPushMatrix();

		// z Axis green
		glMaterialAmbientAndDiffuse(GLMaterialEnums::FRONT,
				GLColor<GLfloat, 4>(0,0,1));
		glDrawArrow(0.05,0.15,0.3,5,30);

		glPopMatrix();
	}
}

// this is from the Calculations program, could be outdated now
VectorXd GlyphDemo::eigenValuesToSuperEllipsoidParameters(Matrix3d Tensor , double sharpnessgamb, double sharpnessgamu) const{

	// TRANSFORM THE MATRIX INTO A TRACELESS TENSOR
	// page 1198
	Tensor = Tensor - 1.0/3.0*MatrixXd::Identity(3,3);

	EigenSolver<Matrix3d> solver( Tensor ); // compute EVals and EVec

	//cout << solver.eigenvalues() << endl;
	double eig1 = solver.eigenvalues()[0].real();//to get the relevant value, the imaginary part should be zero anyway
	double eig2 = solver.eigenvalues()[1].real();
	double eig3 = solver.eigenvalues()[2].real();

	int eig1vec=0; // important when swapping
	int eig2vec=1;
	int eig3vec=2;

	// SORT BY MAGNITUDE
	if (fabs(eig1) > fabs(eig2) ) {
		if (fabs(eig3) > fabs(eig1) ) {	swap(eig1,eig3);	swap(eig1vec,eig3vec);	}
	} else {
		if (fabs(eig2) > fabs(eig3) ) { swap(eig1,eig2); swap(eig1vec,eig2vec); }
		else { swap(eig1,eig3); swap(eig1vec,eig3vec); }
	}
	if(fabs(eig3) > fabs(eig2) ) { swap(eig2,eig3); swap(eig2vec,eig3vec); }

	// TRY USING THE IEEE Paper computations
	// Page 1199, equations 4-7
	double S = 1.5*eig1;
	double muuplus, muuminus, mub, mui;
	if(S>=0){
		muuplus=-3*eig3;
		muuminus=0;
		mui = 1- 1.5*eig1;
	} else {
		muuplus=0;
		muuminus=6*eig3;
		mui = 1+ 3*eig1;
	}
	mub=fabs(3*eig1+6*eig3);

	// equation 8
	double alpha=0, beta=0;

	if(muuplus>=0 && muuminus == 0){
		alpha = pow(1-mub,sharpnessgamb);
		beta = pow(1-muuplus,sharpnessgamu);

	}
	if (muuplus==0 && muuminus > 0) {
		alpha = pow(1-mub,sharpnessgamb);
		beta = 1+3*pow(muuminus,sharpnessgamu);
	}

	// page 1200
	// Evaluate stretch factors
	double strechX=1, strechY=1, strechZ=1;

	// define range of strechfactors
	double strechMax=0.5; // in paper: 0.5, marcel: 1.0
	double strechMin=0.1;

	// equations 9 and 10
	if (muuplus >= muuminus){
		strechX = strechMin + (strechMax-strechMin)*mui;
		strechY = strechMin + (strechMax-strechMin)*(1-muuplus);
	} else {
		strechX = strechMin + (strechMax-strechMin)*(1-0.5*mub);
		strechY=strechMax;
	}
	strechZ=strechMax;


	// according to the source, we must now rotate to eigenvector frame
	// access the columns
	Vector3d eigVector1 = solver.eigenvectors().col(eig1vec).real();
	Vector3d eigVector2 = solver.eigenvectors().col(eig2vec).real();
	Vector3d eigVector3 = solver.eigenvectors().col(eig3vec).real();
	eigVector1.normalize();
	eigVector2.normalize();
	eigVector3.normalize();


	// With change of base matrix http://www.mathworks.com/matlabcentral/newsreader/view_thread/156864
	Matrix3d rot;
	rot <<  eigVector3.x(), eigVector2.x(), eigVector1.x(),
			eigVector3.y(), eigVector2.y(), eigVector1.y(),
			eigVector3.z(), eigVector2.z(), eigVector1.z();

	Quaterniond quat;
	quat=rot;

	//	return the information in the following format:
	//  paramT, ParamT,  xrot, yrot, zrot, wrot, xscale,yscale,zscale,, note: THE POSITION COME FROM THE TENSOR DATA
	VectorXd data;
	data.resize(9);
	data(0)=alpha;
	data(1)=beta;
	data(2)=quat.x();
	data(3)=quat.y();
	data(4)=quat.z();
	data(5)=quat.w();
	data(6)=strechX;
	data(7)=strechY; // Z
	data(8)=strechZ; // Y
	return data;
}

/* Create and execute an application object: */
VRUI_APPLICATION_RUN(GlyphDemo) // this is actually our main() function

///////////////////////
// Init methods
///////////////////////

void GlyphDemo::initContext(GLContextData& contextData) const {
	/*********************************************************************
	 For classes derived from GLObject, this method is called for each
	 newly-created object, once per every OpenGL context created by Vrui.
	 This method must not change application or Vrui state, but only create
	 OpenGL-dependent application data and store them in the GLContextData
	 object for retrieval in the display method.
	 *********************************************************************/
	// This is called AFTER the GlyphDemo constructor!!

	cout << "Init OpenGL context" << endl;

	/* Create context data item and store it in the GLContextData object: */
	DataItem* dataItem = new DataItem;
	contextData.addDataItem(this, dataItem);

// Create and compile our GLSL program from the shaders
	cout << "Loading shaders" << endl;

	dataItem->shaderaddress->initExtensions();
	dataItem->shaderaddress->compileVertexShader(s.vertShaderName.c_str());
	dataItem->shaderaddress->compileFragmentShader(s.fragShaderName.c_str());
	dataItem->shaderaddress->linkShader();
	dataItem->shaderaddress->useProgram();

	cout << "Initializing uniforms" << endl;
	dataItem->LightPosID = dataItem->shaderaddress->getUniformLocation("LightPosition_worldspace");
	dataItem->SpecularColorID = dataItem->shaderaddress->getUniformLocation("SpecularColor");
	dataItem->AmbientColorFactorID = dataItem->shaderaddress->getUniformLocation("AmbientColorFactor");
	dataItem->LightColorID = dataItem->shaderaddress->getUniformLocation("LightColor");
	dataItem->LightPowerID = dataItem->shaderaddress->getUniformLocation("LightPower");

	// Now, send the precomputed glyphs to the gpu buffers
	bindBuffers(dataItem);

	cout << "Init OpenGL context - DONE" << endl;
}

void GlyphDemo::bindBuffers(DataItem* dataItem) const {

	// These are the buffers for the matrices that are sent to the shader
	// They change every frame, but we initialize them once to avoid memory reallocation
	dataItem->MVPs = new std::vector<glm::mat4>[sizeBeta*sizeAlpha];
	dataItem->MVs = new std::vector<glm::mat4>[sizeBeta*sizeAlpha];

	// now we send the data to the buffers
	// determine the size of the buffers
	unsigned int buffersize = 3 * levelOfDetail * levelOfDetail * sizeAlpha * sizeBeta; // 3D * lod^2 vertices * sizeT times sizeR glyphs

	cout << "Binding vertices to buffers" << endl;
	// VERTEX BUFFER
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->vertexbuffer);
	glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(GLfloat)*buffersize,
			g_vertex_buffer_data, GL_STATIC_DRAW_ARB);

	//NORMALS BUFFER
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->normalbuffer);
	glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(GLfloat)*buffersize,
			g_normal_buffer_data, GL_STATIC_DRAW_ARB);

	cout << "Specify vertexbuffer" << endl;
	glEnableVertexAttribArrayARB(0);
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->vertexbuffer);
	glVertexAttribPointerARB(0, // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*) 0            // array buffer offset
			);

	cout << "Specify normalbuffer" << endl;
	glEnableVertexAttribArrayARB(1);
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->normalbuffer);
	glVertexAttribPointerARB(1,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*) 0                          // array buffer offset
			);

	cout << "Specify elementbuffer" << endl;
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, dataItem->elementbuffer);
	glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, sizeOfIndexGrid * sizeof(unsigned int), &indexGrid[0], GL_STATIC_DRAW_ARB);

	cout << "Prepare MVP, MV and color buffers" << endl;
    // for indexed rendering...
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->MVbuffer);

	for (unsigned int i = 0; i < 4 ; i++) {
		glEnableVertexAttribArrayARB(2 + i);
		glVertexAttribPointerARB(2 + i, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4),
								(const GLvoid*)(sizeof(GLfloat) * i * 4));
		glVertexAttribDivisorARB(2 + i, 1);
	}

	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->MVPbuffer);

	for (unsigned int i = 0; i < 4 ; i++) {
		glEnableVertexAttribArrayARB(6 + i);
		glVertexAttribPointerARB(6 + i, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4),
								(const GLvoid*)(sizeof(GLfloat) * i * 4));
		glVertexAttribDivisorARB(6 + i, 1);
	}

	glBindBufferARB(GL_ARRAY_BUFFER_ARB, dataItem->colorbuffer);

	glEnableVertexAttribArrayARB(10);
	glVertexAttribPointerARB(10, 3, GL_FLOAT, GL_FALSE, 0,0);
	glVertexAttribDivisorARB(10, 1);
}

void GlyphDemo::loadPrecomputedGlyphs() {

	cout << "Load the precomputed superellipsoids" << endl;

	// here read the precomputed superellipsoids info file
	// because the prepareIndexGrid function needs the information here
	// we also create the application buffer, but we do not bind to any GPU buffers (that happens in bindBuffer())

	string PreComputedSuperEllipsoidsFileLocation = s.projectData + s.preCompLocation;
	string info = PreComputedSuperEllipsoidsFileLocation + "Info";

	//open file
	ifstream preComputedStream;
	preComputedStream.open(info.c_str());

	//read first lines
	string line;
	// store some settings for later use
	preComputedStream >> line >> minBeta;
	preComputedStream >> line >> maxBeta;
	preComputedStream >> line >> minAlpha;
	preComputedStream >> line >> maxAlpha;
	preComputedStream >> line >> parameterStep;
	preComputedStream >> line >> levelOfDetail;

	// compute total size of the glyph grid
	sizeBeta = round((maxBeta - minBeta) / parameterStep);
	sizeAlpha = round((maxAlpha - minAlpha) / parameterStep);

	std::cout << "Index grid size: " << sizeBeta * sizeAlpha << std::endl;

	//close file
	preComputedStream.close();

	unsigned int buffersize = 3 * levelOfDetail * levelOfDetail * sizeAlpha * sizeBeta; // 3D * lod^2 vertices * sizeT times sizeR glyphs
	cout << "buffersize is = " << buffersize << endl;

	g_vertex_buffer_data = new GLfloat[buffersize];
	g_normal_buffer_data = new GLfloat[buffersize];

	ifstream InFile;
	string vert = PreComputedSuperEllipsoidsFileLocation + "Vert";
	InFile.open(vert.c_str(), ios::in | ios::binary);
	InFile.read( (char*)g_vertex_buffer_data, sizeof(unsigned int)*buffersize);
	InFile.close();
	string norm = PreComputedSuperEllipsoidsFileLocation + "Norm";
	InFile.open(norm.c_str(), ios::in | ios::binary);
	InFile.read( (char*)g_normal_buffer_data, sizeof(unsigned int)*buffersize);
	InFile.close();
}

void GlyphDemo::prepareIndexGrid() {
	// we run this method once to prepare our set of indices pointing
	// into the vertex buffer that is set up in the "bindBuffers" function
	// later, we will use only this index buffer to do the drawing

	// compute sizes of the indexing
	int sizeOfBetaIndexBuffer = levelOfDetail * levelOfDetail * sizeAlpha;
	int sizeOfAlphaIndexbuffer = levelOfDetail * levelOfDetail;

	// loop through parameters, and create the necessary indexing
	for (int indexBeta = 0; indexBeta < sizeBeta; indexBeta++) {

		int offsetBeta = indexBeta * sizeOfBetaIndexBuffer;

		for (int indexAlpha = 0; indexAlpha < sizeAlpha; indexAlpha++) {

			int offsetAlpha = indexAlpha * sizeOfAlphaIndexbuffer;

			int indexLOD = 0;

			// loop through the vertices
			for (int j = 0; j < (levelOfDetail-1); j++) {


				for (int i = 0; i < (levelOfDetail); i++) {
					int indexOffset;

					// TIANGLE_STRIP style - less indices
					indexOffset = offsetBeta + offsetAlpha + (j + 1) * levelOfDetail + (i + 0);
					indexGrid[(indexBeta*sizeAlpha+indexAlpha)*sizeOfEllipsoid+indexLOD] = indexOffset;
					indexLOD++;

					indexOffset = offsetBeta + offsetAlpha + (j + 0) * levelOfDetail + (i + 0);
					indexGrid[(indexBeta*sizeAlpha+indexAlpha)*sizeOfEllipsoid+indexLOD] = indexOffset;
					indexLOD++;
				}

				// we need to restart the triangle strip each time the inner loop is completed
				// without this, the ellipsoid will have weird triangles on one edge
				// see also http://paulbourke.net/geometry/superellipse/
				indexGrid[(indexBeta*sizeAlpha+indexAlpha)*sizeOfEllipsoid+indexLOD] = 0xFFFFFFFF;
				indexLOD++;

			} // end forloop vertices
		}
	} // end for grid
}


///////////////////////
// Stuff related to Vrui Menues
///////////////////////

GLMotif::PopupMenu* GlyphDemo::createMainMenu(void) {
	/* Create a popup shell to hold the main menu: */
	GLMotif::PopupMenu* mainMenuPopup = new GLMotif::PopupMenu("MainMenuPopup",
			Vrui::getWidgetManager());
	mainMenuPopup->setTitle("A.C.T.I.V.E");

	/* Create the main menu itself: */
	GLMotif::Menu* mainMenu = new GLMotif::Menu("MainMenu", mainMenuPopup,
			false);

	/* Create a button: */
	resetNavigationButton = new GLMotif::Button(
			"ResetNavigationButton", mainMenu, "Reset Navigation");

	/* Add a callback to the button: */
	resetNavigationButton->getSelectCallbacks().add(this,
			&GlyphDemo::buttonCallback);
			
	reloadTensorButton = new GLMotif::Button(
			"reloadTensorButton", mainMenu, "ReloadTensor");

	/* Add a callback to the button: */
	reloadTensorButton->getSelectCallbacks().add(this,
			&GlyphDemo::buttonCallback);

	showRenderDialogToggle = new GLMotif::ToggleButton(
			"ShowLightDialogToggle", mainMenu, "Show Render Settings");
	showRenderDialogToggle->setToggle(false);
	showRenderDialogToggle->getValueChangedCallbacks().add(this,
			&GlyphDemo::toggleButtonCallback);

	showGlyphDemoToggle = new GLMotif::ToggleButton(
			"showGlyphDemoToggle", mainMenu, "Show GlyphDemo Settings");
	showGlyphDemoToggle->setToggle(false);
	showGlyphDemoToggle->getValueChangedCallbacks().add(this,
			&GlyphDemo::toggleButtonCallback);

	/* Finish building the main menu: */
	mainMenu->manageChild();

	return mainMenuPopup;
}

GLMotif::PopupWindow* GlyphDemo::createRenderDialog(void) {
	const GLMotif::StyleSheet& ss = *Vrui::getWidgetManager()->getStyleSheet();

	GLMotif::PopupWindow* lightDialogPopup = new GLMotif::PopupWindow(
			"LightDialogPopup", Vrui::getWidgetManager(), "Render settings");
	lightDialogPopup->setResizableFlags(true, false);
	lightDialogPopup->setCloseButton(true);
	lightDialogPopup->getCloseCallbacks().add(this,
			&GlyphDemo::menuCloseCallback);

	GLMotif::RowColumn* lightDialog = new GLMotif::RowColumn(
			"LightDialog", lightDialogPopup, false);
	lightDialog->setNumMinorWidgets(3);

	new GLMotif::Label("LightPowerLabel", lightDialog, "Light power");

	lightPowerValue = new GLMotif::TextField("LightPowerValue",
			lightDialog, 19);
	lightPowerValue->setValue(lightPower);

	lightPowerSlider = new GLMotif::Slider("LightPowerSlider",
			lightDialog, GLMotif::Slider::HORIZONTAL,
			ss.fontHeight * 15.0f);
	lightPowerSlider->setValueRange(0,20,0.1);
	lightPowerSlider->setValue(lightPower);
	lightPowerSlider->getValueChangedCallbacks().add(this,
			&GlyphDemo::sliderCallback);

	new GLMotif::Label("glyphScalingFactorL", lightDialog, "Glyphs scaling");

	glyphScalingFactorValue = new GLMotif::TextField("glyphScalingFactorValue",
			lightDialog, 19);
	glyphScalingFactorValue->setValue(glyphScalingFactor);

	glyphScalingFactorSlider = new GLMotif::Slider("glyphScalingFactorSlider",
			lightDialog, GLMotif::Slider::HORIZONTAL,
			ss.fontHeight * 15.0f);
	glyphScalingFactorSlider->setValueRange(0.01,2,0.01);
	glyphScalingFactorSlider->setValue(glyphScalingFactor);
	glyphScalingFactorSlider->getValueChangedCallbacks().add(this,
			&GlyphDemo::sliderCallback);

	new GLMotif::Label("zAmplificationFactorValueL", lightDialog, "z-Axis amplification");

	zAmplificationFactorValue = new GLMotif::TextField("zAmplificationFactorValue",
			lightDialog, 19);
	zAmplificationFactorValue->setValue(zAmplificationFactor);

	zAmplificationSlider = new GLMotif::Slider("zAmplificationSlider",
			lightDialog, GLMotif::Slider::HORIZONTAL,
			ss.fontHeight * 15.0f);
	zAmplificationSlider->setValueRange(1,3,0.01);
	zAmplificationSlider->setValue(zAmplificationFactor);
	zAmplificationSlider->getValueChangedCallbacks().add(this,
			&GlyphDemo::sliderCallback);

	wireframeToggle = new GLMotif::ToggleButton("wireframeToggle", lightDialog,
			"Toggle wireframe");
	wireframeToggle->setToggle(wireframe);
	wireframeToggle->getValueChangedCallbacks().add(this,
			&GlyphDemo::toggleButtonCallback);
			
	fillToggle = new GLMotif::ToggleButton("fillToggle", lightDialog,
			"Toggle fill polygon");
	fillToggle->setToggle(fill);
	fillToggle->getValueChangedCallbacks().add(this,
			&GlyphDemo::toggleButtonCallback);
			
	coordSystemToggle = new GLMotif::ToggleButton("coordSystemToggle", lightDialog,
			"Toggle coordinate system");
	coordSystemToggle->setToggle(coordSystem);
	coordSystemToggle->getValueChangedCallbacks().add(this,
			&GlyphDemo::toggleButtonCallback);

	lightDialog->manageChild();

	return lightDialogPopup;
}

GLMotif::PopupWindow* GlyphDemo::createGlyphDemoDialog(void) {
	const GLMotif::StyleSheet& ss = *Vrui::getWidgetManager()->getStyleSheet();

	GLMotif::PopupWindow* glyphDemoPopup = new GLMotif::PopupWindow(
			"glyphDemoPopup", Vrui::getWidgetManager(), "Glyph Demo");
	glyphDemoPopup->setResizableFlags(true, false);
	glyphDemoPopup->setCloseButton(true);
	glyphDemoPopup->getCloseCallbacks().add(this,
			&GlyphDemo::menuCloseCallback);

	GLMotif::RowColumn* glyphDialog = new GLMotif::RowColumn(
			"glyphDialog", glyphDemoPopup, false);
	glyphDialog->setNumMinorWidgets(3);

	new GLMotif::Label("alphaLabel", glyphDialog, "Alpha");
	alphaValue = new GLMotif::TextField("alphaValue",
			glyphDialog, 19);

	alphaSlider = new GLMotif::Slider("alphaSlider",
			glyphDialog, GLMotif::Slider::HORIZONTAL,
			ss.fontHeight * 15.0f);
	alphaSlider->setValueRange(minAlpha, maxAlpha, parameterStep);
	alphaSlider->setValue(alpha);
	alphaSlider->getValueChangedCallbacks().add(this,
			&GlyphDemo::sliderCallback);

	new GLMotif::Label("betaLabel", glyphDialog, "Beta");
	betaValue = new GLMotif::TextField("betaValue",
			glyphDialog, 19);
	updateAlphaBeta();

	betaSlider = new GLMotif::Slider("betaSlider",
			glyphDialog, GLMotif::Slider::HORIZONTAL,
			ss.fontHeight * 15.0f);
	betaSlider->setValueRange(minBeta, maxBeta, parameterStep);
	betaSlider->setValue(alpha);
	betaSlider->getValueChangedCallbacks().add(this,
			&GlyphDemo::sliderCallback);
			
	tensorTestToggle = new GLMotif::ToggleButton("tensorTestToggle", glyphDialog,
			"Toggle customTensor");
	tensorTestToggle->setToggle(tensorTest);
	tensorTestToggle->getValueChangedCallbacks().add(this,
			&GlyphDemo::toggleButtonCallback);

	glyphDialog->manageChild();

	return glyphDemoPopup;
}

void GlyphDemo::updateAlphaBeta() {
	if (alphaValue!=NULL)
		alphaValue->setValue(alpha);
	if (betaValue!=NULL)
		betaValue->setValue(beta);
}

void GlyphDemo::resetNavigation() {
	/* Set the navigation transformation: */
	Vrui::NavTransform t = Vrui::NavTransform::identity;
	t *= Vrui::NavTransform::translateFromOriginTo(Vrui::getDisplayCenter());
	t *= Vrui::NavTransform::scale(s.meterFactorCoefficient * Vrui::getMeterFactor()); // we assume our nav space unit is one meter * meterFactorCoefficient
	Vrui::setNavigationTransformation(t);
}

void GlyphDemo::reloadTensor() {
	ifstream stream;
	stream.open("tensor");
	
	std::string line;
	std::getline(stream,line);
	std::stringstream bla1(line);
	double aa,ab,ac,ba,bb,bc,ca,cb,cc;
	bla1 >> aa;
	bla1 >> ab;
	bla1 >> ac;
	
	std::getline(stream,line);
	std::stringstream bla2(line);
	bla2 >> ba;
	bla2 >> bb;
	bla2 >> bc;
	
	std::getline(stream,line);
	std::stringstream bla3(line);
	bla3 >> ca;
	bla3 >> cb;
	bla3 >> cc;
	
	tensor << aa,ab,ac,
		ba,bb,bc,
		ca,cb,cc;
	
	stream.close();
	
	// Debug output
	VectorXd tensorIntel=eigenValuesToSuperEllipsoidParameters(tensor, 3, 3);	
	std::cout << "----------------------------------" << std::endl;
	std::cout << "Tensor properties:" << std::endl;
	std::cout << "Alpha: " << tensorIntel(0) << std::endl;
	std::cout << "Beta: " << tensorIntel(1) << std::endl;
	std::cout << "xQuaternion: " << tensorIntel(2) << std::endl;
	std::cout << "yQuaternion: " << tensorIntel(3) << std::endl;
	std::cout << "zQuaternion: " << tensorIntel(4) << std::endl;
	std::cout << "wQuaternion: " << tensorIntel(5) << std::endl;
	std::cout << "xScale: " << tensorIntel(6) << std::endl;
	std::cout << "yScale: " << tensorIntel(7) << std::endl;
	std::cout << "zScale: " << tensorIntel(8) << std::endl;
	std::cout << "----------------------------------" << std::endl << std::endl;
}

void GlyphDemo::toggleButtonCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData){
	if (cbData->toggle == wireframeToggle) {
		wireframe=cbData->set;
	} else if (cbData->toggle == fillToggle) {
		fill=cbData->set;
	} else if (cbData->toggle == coordSystemToggle) {
		coordSystem=cbData->set;
	}else if (cbData->toggle == tensorTestToggle) {
		tensorTest=cbData->set;
	} else if (cbData->toggle == showRenderDialogToggle) {
		if (cbData->set) {
			Vrui::popupPrimaryWidget(renderDialog);
		} else {
			Vrui::popdownPrimaryWidget(renderDialog);
		}
	} else if (cbData->toggle == showGlyphDemoToggle) {
		if (cbData->set) {
			Vrui::popupPrimaryWidget(glyphDemoDialog);
		} else {
			Vrui::popdownPrimaryWidget(glyphDemoDialog);
		}
	}
}

void GlyphDemo::buttonCallback(GLMotif::Button::SelectCallbackData* cbData) {
	if (cbData->button == resetNavigationButton) {
		/* Reset the Vrui navigation transformation: */
		resetNavigation();
	} else if (cbData->button == reloadTensorButton) {
		reloadTensor();
	}
}

void GlyphDemo::sliderCallback(GLMotif::Slider::ValueChangedCallbackData* cbData){
	if (cbData->slider == lightPowerSlider) {
		lightPower=cbData->value;
		lightPowerValue->setValue(lightPower);
	} else if (cbData->slider == glyphScalingFactorSlider) {
		glyphScalingFactor=cbData->value;
		glyphScalingFactorValue->setValue(glyphScalingFactor);
	} else if (cbData->slider == zAmplificationSlider) {
		zAmplificationFactor=cbData->value;
		zAmplificationFactorValue->setValue(zAmplificationFactor);
	} else if (cbData->slider == alphaSlider) {
		alpha=cbData->value;
		updateAlphaBeta();
	} else if (cbData->slider == betaSlider) {
		beta=cbData->value;
		updateAlphaBeta();
	}
}

void GlyphDemo::menuCloseCallback(GLMotif::PopupWindow::CloseCallbackData* cbData){
	if (cbData->popupWindow == renderDialog) {
		showRenderDialogToggle->setToggle(false);
	} else if (cbData->popupWindow == glyphDemoDialog) {
		showGlyphDemoToggle->setToggle(false);
	}
}
