#ifndef GlyphDemo_H_
#define GlyphDemo_H_

///////////////////
// Includes
///////////////////

// OGL
#include <GL/gl.h>

// Vrui
#include <Math/Math.h>

#include <Vrui/Vrui.h>
#include <Vrui/Application.h>

#include <Geometry/OrthogonalTransformation.h>

#include <GL/GLShader.h>
#include <GL/GLMaterialTemplates.h>
#include <GL/GLModels.h>
#include <GL/GLColorTemplates.h>
#include <GL/GLVertexTemplates.h>
#include <GL/GLObject.h>
#include <GL/GLContextData.h>
#include <GL/GLGeometryWrappers.h>

#include <GL/Extensions/GLExtension.h>
#include <GL/Extensions/GLARBDrawInstanced.h>
#include <GL/Extensions/GLARBInstancedArrays.h>
#include <GL/Extensions/GLARBVertexBufferObject.h>
#include <GL/Extensions/GLARBVertexProgram.h>
#include <GL/Extensions/GLARBVertexShader.h>
#include <GL/Extensions/GLARBFragmentShader.h>
#include <GL/Extensions/GLARBShaderObjects.h>

#include <GLMotif/Button.h>
#include <GLMotif/Menu.h>
#include <GLMotif/PopupMenu.h>
#include <GLMotif/PopupWindow.h>
#include <GLMotif/ToggleButton.h>
#include <GLMotif/WidgetManager.h>
#include <GLMotif/TextField.h>
#include <GLMotif/Slider.h>
#include <GLMotif/StyleSheet.h>

// GLM
#define GLM_FORCE_RADIANS
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Eigen
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Geometry>

// Standard library
#include <math.h> // use M_PI from here
#include <vector>
#include <fstream>
#include <dirent.h>
#include <map>

// Other
#include "GlobalSettings.h"

using namespace glm;
using namespace std;
using namespace Eigen;

class GlyphDemo: public Vrui::Application, public GLObject {
public:
	struct DataItem: public GLObject::DataItem // Data structure storing OpenGL-dependent application data
	{
	public:
		// =============================
		// glyph stuff
		// buffer ids
		GLuint vertexbuffer;
		GLuint normalbuffer;
		GLuint elementbuffer;
		// the following three are instance vertex buffers
		GLuint MVbuffer;
		GLuint MVPbuffer;
		GLuint colorbuffer;

		GLShader* shaderaddress;

		GLuint LightPosID;
		GLuint SpecularColorID;
		GLuint AmbientColorFactorID;
		GLuint LightPowerID;
		GLuint LightColorID;

		// These are the matrices and vectors per instance that are sent to the shader
		std::vector<glm::mat4> *MVPs;
		std::vector<glm::mat4> *MVs;

		/* Constructors and destructors: */
		DataItem(void) {
			/* Initialize the GL_ARB_vertex_buffer_object extension: */
			GLARBVertexBufferObject::initExtension();
			// And other extensions for instance rendering
			GLARBDrawInstanced::initExtension();
			GLARBInstancedArrays::initExtension();

			// generate the buffers
			glGenBuffersARB(1, &vertexbuffer);
			glGenBuffersARB(1, &normalbuffer);
			glGenBuffersARB(1, &elementbuffer);
			glGenBuffersARB(1, &MVbuffer);
			glGenBuffersARB(1, &MVPbuffer);
			glGenBuffersARB(1, &colorbuffer);

			MVPs = NULL; // initialized in initBuffers
			MVs = NULL;
			shaderaddress = new GLShader;
		}
		;
		virtual ~DataItem(void) {
			// generate the buffers
			glDeleteBuffersARB(1, &vertexbuffer);
			glDeleteBuffersARB(1, &normalbuffer);
			glDeleteBuffersARB(1, &elementbuffer);
			glDeleteBuffersARB(1, &MVbuffer);
			glDeleteBuffersARB(1, &MVPbuffer);
			glDeleteBuffersARB(1, &colorbuffer);

			delete[] MVPs;
			delete[] MVs;
			delete shaderaddress;
		}
		;
	};

	GlyphDemo(int& argc, char**& argv); // Initializes the Vrui toolkit and the application
	virtual ~GlyphDemo(void); // Shuts down the Vrui toolkit

	/* Methods from Vrui::Application: */
	virtual void frame(void); // Called exactly once per frame
	virtual void display(GLContextData& contextData) const; // Called for every eye and every window on every frame

	/* Methods from GLObject: */
	virtual void initContext(GLContextData& contextData) const; // Called once upon creation of each OpenGL context

private:

	////////////////////
	// Members
	////////////////////

	// Data for displaying
	unsigned int* indexGrid; // indices into the vertex buffer, indexed by the alpha and beta values
	unsigned int sizeOfIndexGrid; // = sizeOfEllipsoids*sizeAlpha*sizeBeta
	unsigned int sizeOfEllipsoid; // how many indices are needed to render one ellipsoid - depends on indexing type and levelofdetail
	// Information for indexGrid - these come from the precomputed superellipsoids file
	double minBeta, maxBeta, minAlpha, maxAlpha, parameterStep;
	int levelOfDetail, sizeBeta, sizeAlpha;

	
	// Other stuff important for rendering
	glm::vec4 lightPos;
	float lightPower;
	double glyphScalingFactor; // scale glyphs on every axis - controlled by user
	double zAmplificationFactor; // scale glyphs on z axis (which is aligned with the biggest eigenvector) - controlled by user
	bool wireframe;
	bool fill;
	bool tensorTest;
	bool coordSystem;
	Matrix3d tensor;
	
	// These are the buffers for the precomputed vertices (they are sent per context to the GPU, but also stored per application here)
	GLfloat *g_vertex_buffer_data;
	GLfloat *g_normal_buffer_data;

	double alpha; // alpha and beta of the rendered glyph - controlled by the user
	double beta;
	GLMotif::ToggleButton* showGlyphDemoToggle;
	GLMotif::PopupWindow* glyphDemoDialog;
	GLMotif::TextField* alphaValue;
	GLMotif::Slider* alphaSlider;
	GLMotif::TextField* betaValue;
	GLMotif::Slider* betaSlider;
	GLMotif::PopupWindow* createGlyphDemoDialog(void);
	void updateAlphaBeta();
	
	VectorXd eigenValuesToSuperEllipsoidParameters(Matrix3d Tensor , double sharpnessgamb, double sharpnessgamu) const;

	// Stuff related to Vrui Menues
	GLMotif::PopupMenu* mainMenu; // The program's main menu
	GLMotif::PopupWindow* renderDialog; // guess what :D

	GLMotif::Button* resetNavigationButton; // reset the VRUI transform so that the mesh is aligned at the origin
	GLMotif::Button* reloadTensorButton;

	GLMotif::ToggleButton* showRenderDialogToggle; // button in the main menu
	GLMotif::ToggleButton* wireframeToggle;
	GLMotif::ToggleButton* fillToggle;
	GLMotif::ToggleButton* tensorTestToggle;
	GLMotif::ToggleButton* coordSystemToggle;
	
	GLMotif::Slider* lightPowerSlider; // influences specular and diffuse color
	GLMotif::Slider* glyphScalingFactorSlider; // stretch the glyphs in all directions
	GLMotif::Slider* zAmplificationSlider; // stretch the glyphs only in the direction of the biggest eigenvector

	// the textfields show the sliders' values
	GLMotif::TextField* lightPowerValue;
	GLMotif::TextField* glyphScalingFactorValue;
	GLMotif::TextField* zAmplificationFactorValue;


	////////////////////
	// Methods
	////////////////////

	// Init methods
	void loadPrecomputedGlyphs();
	void prepareIndexGrid();
	void bindBuffers(DataItem* dataItem) const;

	// Stuff related to Vrui Menues
	GLMotif::PopupMenu* createMainMenu(void); // Creates the program's main menu
	GLMotif::PopupWindow* createRenderDialog(void);

	void toggleButtonCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData);
	void buttonCallback(GLMotif::Button::SelectCallbackData* cbData);
	void sliderCallback(GLMotif::Slider::ValueChangedCallbackData* cbData);
	void menuCloseCallback(GLMotif::PopupWindow::CloseCallbackData* cbData);

	void resetNavigation();
	void reloadTensor();
};

#endif /* GlyphDemo_H_ */
