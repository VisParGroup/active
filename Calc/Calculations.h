/*
 * Calculations.h
 *
 *  Created on: Sep 18, 2014
 *      Author: internshipdude
 */

#ifndef CALCULATIONS_H_
#define CALCULATIONS_H_

#include <eigen3/Eigen/Dense>
using namespace Eigen;

// when calculating the normals, NANs, INFs and other ugly things appear
// this function caps them to 0 or 100
void capValues(glm::dvec3&);

// Calculate a vertex for a superellipsoid
// from IEEE paper
// 0 < alpha, beta < inf; -pi <= theta <= pi; -pi/2 <= phi <= pi/2
glm::dvec3 superParameterCoord(double theta, double phi, double alpha, double beta);

// Calculate Superellipsoid parameters based on tensor data and sharpness
VectorXd eigenValuesToSuperEllipsoidParameters(Matrix3d Tensor , double sharpnessgamb, double sharpnessgamu);


#endif /* CALCULATIONS_H_ */
