/*
 * Tensors.h
 *
 *  Created on: Sep 18, 2014
 *      Author: internshipdude
 */

#ifndef TENSORS_H_
#define TENSORS_H_

#include <eigen3/Eigen/Dense>
#include <string>

using namespace std;
using namespace Eigen;

void clearSuperEllipsoidStorage();

void precomputeSuperEllipsoids();

void saveTimeStepToFile(const char* Location);

bool readTensorFileAndCreateEllipsoids(string filecode, const Matrix<double, 3, Dynamic> &TensorPositions, const Matrix<double , 1, Dynamic> &CellSizeStorage );

void prepareTimeSteps();

#endif /* TENSORS_H_ */
