//============================================================================
// Name        : OpenFoamReader.cpp
// Author      : Marcel Padilla
// Version     :
// Copyright   : http://www.wtfpl.net/
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "OpenFoamReader.h"
#include "GlobalSettings.h"

#include <iostream>
#include <stdio.h>
#include <fstream>
#include  <string>

// EIGEN
#include <eigen3/Eigen/Dense>

using namespace std;
using namespace Eigen;

Matrix<int, 1 , Dynamic> FOAM_OwnerStorage;
Matrix<double, 3 , Dynamic> FOAM_PointsStorage;
Matrix<double, 3 , Dynamic> FOAM_FacesStorage;
Matrix<int, 1 , Dynamic> FOAM_NeighbourStorage;
Matrix<Matrix3d, 1 , Dynamic> FOAM_TensorStorage;
Matrix<double, 3 , Dynamic> FOAM_FinalTensorPositionStorage;
Matrix<double, Dynamic, Dynamic> FOAM_CellSizes;
int numberOfPoints, numberOfCellLines, numberOfFaces, numberOfNeighours;

// the ultimate strings that will be used everywhere to read lines
string line, line2 , line3;

//=================================================================================================
//=================================================================================================
//=================================================================================================
// METHODS
//=================================================================================================
//=================================================================================================
//=================================================================================================

int readOpenFoamFile (ifstream* s) {
	int res = -1;
	do {
		getline (*s, line);
		try {
			res = std::stoi(line);
		} catch (exception e) {
			res = -1;
		}
	} while (res == -1);
	getline(*s, line);
	return res;
}


void prepareOwnerFile(){
	// start reading and goto first line
	ifstream FOAM_OwnerStream;
	FOAM_OwnerStream.open(s.projectData + s.ownerLocation);

	numberOfFaces = readOpenFoamFile(&FOAM_OwnerStream);
	std::cout << "Number of faces: " << numberOfFaces << std::endl;

	FOAM_OwnerStorage.resize(1,numberOfFaces);
	FOAM_FacesStorage.resize(3,numberOfFaces);


	// flush in all the Data
	numberOfCellLines = 0;
	for(int i=0 ; i < numberOfFaces ; i++){
		int out;
		FOAM_OwnerStream >> out;
		FOAM_OwnerStorage(0,i) = out;
		if (out > numberOfCellLines)
			numberOfCellLines = out;
	}
	numberOfCellLines++;
	FOAM_TensorStorage.resize(1,numberOfCellLines);
	std::cout << "Number of cells: " << numberOfCellLines << std::endl;

	FOAM_OwnerStream.close(); // close file
}

void preparePointFile(){
	// open file
	ifstream FOAM_PointsStream;
	FOAM_PointsStream.open(s.projectData + s.pointsLocation);

	numberOfPoints = readOpenFoamFile(&FOAM_PointsStream);
	std::cout << "Number of points: " << numberOfPoints << std::endl;

	FOAM_PointsStorage.resize(3,numberOfPoints);

	// flush in all points
	for(int i = 0 ; i < numberOfPoints ; i++){

		FOAM_PointsStream >> line >> line2 >> line3; // read the line

		line.erase(0,1); // erase brackets
		line3.erase(line3.length()-1,1);

		//convert to double & save
		FOAM_PointsStorage(0,i) = atof(line.c_str());
		FOAM_PointsStorage(1,i) = atof(line2.c_str());
		FOAM_PointsStorage(2,i) = atof(line3.c_str());

	}
	// close file
	FOAM_PointsStream.close();
	//	cout << FOAM_PointStorage(0,numberOfPoints-1) << " / "<< FOAM_PointStorage(1,numberOfPoints-1) << " / " << FOAM_PointStorage(2,numberOfPoints-1) << endl;
	//	cout << FOAM_PointStorage(0,0) + FOAM_PointStorage(1,0) +FOAM_PointStorage(2,0) << endl;
}

void prepareFaceFile(){
	// open file
	ifstream FOAM_FacesStream;
	FOAM_FacesStream.open(s.projectData + s.facesLocation);

	readOpenFoamFile(&FOAM_FacesStream);

	// flush in all points
	for(int i = 0 ; i < numberOfFaces ; i++){

		FOAM_FacesStream >> line >> line2 >> line3; // read the line

		line.erase(0,2); // erase brackets & the first awkward number in this file
		line3.erase(line3.length()-1,1);

		//convert to double & save
		FOAM_FacesStorage(0,i) = atoi(line.c_str());
		FOAM_FacesStorage(1,i) = atoi(line2.c_str());
		FOAM_FacesStorage(2,i) = atoi(line3.c_str());

	}

	// close file
	FOAM_FacesStream.close();
}

void prepareNeighbourFile(){
	// open file
	ifstream FOAM_NeighbourStream;
	FOAM_NeighbourStream.open(s.projectData + s.neighbourLocation);

	numberOfNeighours = readOpenFoamFile(&FOAM_NeighbourStream);
	std::cout << "Number of neighbours: " << numberOfNeighours << std::endl;

	FOAM_NeighbourStorage.resize(1,numberOfNeighours);

	// flush in all the Data
	for(int i=0 ; i < numberOfNeighours; i++){
		FOAM_NeighbourStream >> FOAM_NeighbourStorage(0,i);
	}

	// close file
	FOAM_NeighbourStream.close();
}

bool containsThisPoint(int facePointIndex, MatrixXi mat){
	bool contains=false;
	for(int i = 0 ; i < mat.cols() ; i++){
		if( facePointIndex == mat(0,i) ){
			contains=true;
			break;
		}
	}
	return contains;

}

void calculateFinalTensorPositions(){

	FOAM_FinalTensorPositionStorage.resize(3,numberOfCellLines);

	// we need a list of points for each cell to check if the new face has already known points,
	// At the end, store the average of these points
	Matrix<Matrix<int, Dynamic, Dynamic>, Dynamic , Dynamic> knownPositions; // save the index/line of the already read point

	knownPositions.resize(1,numberOfCellLines);

	int neighbourcellIndex, cellIndex, facePointIndex;
	for( int i = 0 ; i < numberOfFaces ; i++ ) {// read the faces, and save every Point once, then add it to the FinalPositionTensor

		cellIndex = FOAM_OwnerStorage(0,i); // this identifies which cell we are talking about
		if(i < numberOfNeighours) neighbourcellIndex = FOAM_NeighbourStorage(0,i); // this identifies which cell of the neighbour with the same face

		for( int facePoints = 0 ; facePoints < 3 ; facePoints++ ) { // iterate thorugh the 3 point of every face

			facePointIndex = FOAM_FacesStorage(facePoints,i); // get the index of a point of a face

			if(!containsThisPoint(facePointIndex, knownPositions(0,cellIndex))){ // check if this point is known, if not, add it
				knownPositions(0,cellIndex).conservativeResize(1, knownPositions(0,cellIndex).cols() + 1); // increase size by one
				knownPositions(0,cellIndex)(0, knownPositions(0,cellIndex).cols() - 1) = facePointIndex;
			}

			// NOW REPEAT THIS FOR THE NEIGHBOUR CELL THAT SHARES THE SAME FACE STOP, they have different sizes, less neighbours than owners
			if(i < numberOfNeighours){

				if(!containsThisPoint(facePointIndex, knownPositions(0,neighbourcellIndex))){ // check if this point is known, if not, add it
					knownPositions(0,neighbourcellIndex).conservativeResize(1, knownPositions(0,neighbourcellIndex).cols() + 1); // increase size by one
					knownPositions(0,neighbourcellIndex)(0, knownPositions(0,neighbourcellIndex).cols() - 1) = facePointIndex;
				}
			}// end neighbours, the rest is boundaries, we do not need more calculations for boundaries

		} // end iteration through the 3 points of the face




		for( int facePoints = 0 ; facePoints < 3 ; facePoints++ ) { // iterate thorugh the 3 point of every face

			facePointIndex = FOAM_FacesStorage(facePoints,i); // get the index of a point of a face


		} // end iteration through the 3 points of the face

	} // end extraction of pointdata


	// Calculate center point
	for( int cell = 0 ; cell < numberOfCellLines ; cell++ ) { // save the average of all points for each cell
		//		if( knownPositions(0,cell).cols() == 0 ) cout << knownPositions(0,cell).cols() << " at cell " << cell << endl;
		for(int knownindex = 0 ; knownindex < knownPositions(0,cell).cols() ; knownindex++){ // loop all indexes
			FOAM_FinalTensorPositionStorage.col(cell) = FOAM_FinalTensorPositionStorage.col(cell) + FOAM_PointsStorage.col(knownPositions(0,cell)(0,knownindex))/(double)knownPositions(0,cell).cols();
		}
	}

	// Calculate cell size

	FOAM_CellSizes.resize(1,numberOfCellLines);
	for( int cell = 0 ; cell < numberOfCellLines ; cell++ ) { // save the max distance inbetween the cell points
		FOAM_CellSizes(0,cell)=-1;
		// loop thorugh indices, testint all possible pairs
		for(int knownindexONE = 0 ; knownindexONE < knownPositions(0,cell).cols() ; knownindexONE++){ // loop all indexes
			for(int knownindexTWO = 0 ; knownindexTWO < knownPositions(0,cell).cols() ; knownindexTWO++){ // loop all indexes
				// calcuate the distance between these two points
				double distance = ( FOAM_PointsStorage.col( knownPositions(0,cell)(0,knownindexONE) ) - FOAM_PointsStorage.col(knownPositions(0,cell)(0,knownindexTWO))).norm();

				// check if this is bigger then the previous
				if( FOAM_CellSizes(0,cell) < distance) FOAM_CellSizes(cell)=distance;
			}
		}
	}


}

void writeFinalTensorPositions(){

	//open file
	ofstream FOAM_FinalTensorPostitionStream;
	FOAM_FinalTensorPostitionStream.open(s.projectData + s.tensorPositionFile);

	// first line is number of points:
	FOAM_FinalTensorPostitionStream << numberOfCellLines << endl;

	double maxX, maxY, maxZ, minX, minY, minZ;
	maxX = maxY = maxZ = minX = minY = minZ = 0;

	for( int cell = 0 ; cell < numberOfCellLines ; cell++ ) { // save the average of all points for each cell
		// Every 3 Lines make up 1 Point!
		FOAM_FinalTensorPostitionStream << FOAM_FinalTensorPositionStorage(0,cell) << endl;
		FOAM_FinalTensorPostitionStream << FOAM_FinalTensorPositionStorage(1,cell) << endl;
		FOAM_FinalTensorPostitionStream << FOAM_FinalTensorPositionStorage(2,cell) << endl;

		if (FOAM_FinalTensorPositionStorage(0,cell) > maxX)
			maxX = FOAM_FinalTensorPositionStorage(0,cell);
		if (FOAM_FinalTensorPositionStorage(0,cell) < minX)
			minX = FOAM_FinalTensorPositionStorage(0,cell);
		if (FOAM_FinalTensorPositionStorage(1,cell) > maxY)
			maxY = FOAM_FinalTensorPositionStorage(1,cell);
		if (FOAM_FinalTensorPositionStorage(1,cell) < minY)
			minY = FOAM_FinalTensorPositionStorage(1,cell);
		if (FOAM_FinalTensorPositionStorage(2,cell) > maxZ)
			maxZ = FOAM_FinalTensorPositionStorage(2,cell);
		if (FOAM_FinalTensorPositionStorage(2,cell) < minZ)
			minZ = FOAM_FinalTensorPositionStorage(2,cell);
	}


	//close file
	FOAM_FinalTensorPostitionStream.close();

	//open file
	ofstream FOAM_FinalTensorBoundsStream;
	FOAM_FinalTensorBoundsStream.open(s.projectData + s.tensorBounds);

	FOAM_FinalTensorBoundsStream << minX << endl;
	FOAM_FinalTensorBoundsStream << minY << endl;
	FOAM_FinalTensorBoundsStream << minZ << endl;
	FOAM_FinalTensorBoundsStream << maxX << endl;
	FOAM_FinalTensorBoundsStream << maxY << endl;
	FOAM_FinalTensorBoundsStream << maxZ << endl;

	FOAM_FinalTensorBoundsStream.close();

//	std::cout << minX << ", " << minY << ", " << minZ << endl;
//	std::cout << maxX << ", " << maxY << ", " << maxZ << endl;
//	double toMinX = maxX-minX;
//	double toMinY = maxY-minY;
//	double toMinZ = maxZ-minZ;
//	toMinX/=2;
//	toMinY/=2;
//	toMinZ/=2;
//	double pX = minX+toMinX;
//	double pY = minY+toMinY;
//	double pZ = minZ+toMinZ;
//	std::cout << pX << ", " << pY << ", " << pZ;
}

void writeCellSizeData(){
	// for each cell, evaluate the size of

	//open file
		ofstream FOAM_CellSizeStream;
		FOAM_CellSizeStream.open(s.projectData + s.cellSizesFile);

		// first line is number of points:
		FOAM_CellSizeStream << numberOfCellLines << endl;

		for( int cell = 0 ; cell < numberOfCellLines ; cell++ ) { // save the average of all points for each cell
			// Every Line defines 1 size!
			FOAM_CellSizeStream << FOAM_CellSizes(0,cell) << endl;

		}


		//close file
		FOAM_CellSizeStream.close();
}
