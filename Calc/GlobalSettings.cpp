#include "GlobalSettings.h"
#include <iostream>

Settings s;

int parseSettings(const char* file) {

    SettingsParser settings;
    if(!settings.loadFromFile(file))
    {
        std::cout << "Error loading settings file!" << std::endl;
        return -1;
    }

    settings.get("projectData", s.projectData);
    settings.get("preCompLocation", s.preCompLocation);
    settings.get("vertexDataFolder", s.vertexDataFolder);
    settings.get("tensorBounds", s.tensorBounds);
    settings.get("cellSizesFile", s.cellSizesFile);
    settings.get("tensorPositionFile", s.tensorPositionFile);

    settings.get("ownerLocation", s.ownerLocation);
    settings.get("pointsLocation", s.pointsLocation);
    settings.get("facesLocation", s.facesLocation);
    settings.get("neighbourLocation", s.neighbourLocation);

    settings.get("RunOpenFoamReader", s.RunOpenFoamReader);
    settings.get("PrecomputedEllipsoids", s.PrecomputedEllipsoids);
    settings.get("ComputeEllipsoidDetails", s.ComputeEllipsoidDetails);

    settings.get("CellSizeThreshold", s.CellSizeThreshold);
    settings.get("GlyphAlphaThreshold", s.GlyphAlphaThreshold);
    settings.get("TensorSharpnessGammaB", s.TensorSharpnessGammaB);
    settings.get("TensorSharpnessGammaU", s.TensorSharpnessGammaU);

    settings.get("singlefile", s.singlefile);

    settings.get("minAlpha", s.minAlpha);
    settings.get("maxAlpha", s.maxAlpha);
    settings.get("minBeta", s.minBeta);
    settings.get("maxBeta", s.maxBeta);
    settings.get("parameterStep", s.parameterStep);

    settings.get("levelOfDetail", s.levelOfDetail);
    settings.get("Tensorscale", s.Tensorscale);

    settings.get("clipModelMinX", s.clipModelMinX);
    settings.get("clipModelMinY", s.clipModelMinY);
    settings.get("clipModelMinZ", s.clipModelMinZ);
    settings.get("clipModelMaxX", s.clipModelMaxX);
    settings.get("clipModelMaxY", s.clipModelMaxY);
    settings.get("clipModelMaxZ", s.clipModelMaxZ);

    return 0;
}
