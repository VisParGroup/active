/*
 * Tensors.cpp
 *
 *  Created on: Sep 18, 2014
 *      Author: internshipdude
 */

#include "Tensors.h"
#include "GlobalSettings.h"

#include <GL/gl.h>
#include <vector>
#include <math.h> // use M_PI from here
#include <eigen3/Eigen/Dense>
#include <glm/glm.hpp>
#include <dirent.h>

#include <iostream>
#include <fstream>

#include "Calculations.h"

using namespace std;
using namespace Eigen;

int sizeBeta, sizeAlpha ;
bool clipModel = false;
std::vector<double*> EllipsoidDetails;

void clearSuperEllipsoidStorage(){
	for (std::vector<double*>::iterator it =
			EllipsoidDetails.begin(); it != EllipsoidDetails.end(); ++it) {
		delete [](*it);
	}
	EllipsoidDetails.clear();
}

void precomputeSuperEllipsoids(){
	// this script only has to run once to create VertexFiles of distinct discrete parameters which can be used
	// to approximate other tensor with very similar parameters.
	// We store them in:
	string info = s.projectData + s.preCompLocation + "Info";

	// open file
	ofstream preComputedStream;
	preComputedStream.open(info.c_str());

	std::cout << " - precomputing superellipsoids " << endl;

	// write first lines
	preComputedStream << "minBeta= ";
	preComputedStream << s.minBeta << endl;
	preComputedStream << "maxBeta= ";
	preComputedStream << s.maxBeta << endl;
	preComputedStream << "minAlpha= ";
	preComputedStream << s.minAlpha << endl;
	preComputedStream << "maxAlpha= ";
	preComputedStream << s.maxAlpha << endl;
	preComputedStream << "parameterStep= ";
	preComputedStream << s.parameterStep << endl;
	preComputedStream << "levelOfDetail= ";
	preComputedStream << s.levelOfDetail << endl;

	//close file
	preComputedStream.close();

	int sizeBeta = round((s.maxBeta - s.minBeta) / s.parameterStep);
	int sizeAlpha = round((s.maxAlpha - s.minAlpha) / s.parameterStep);

	// determine the size of the buffers
	unsigned int buffersize = 3 * s.levelOfDetail * s.levelOfDetail * sizeAlpha * sizeBeta; // 3D * lod^2 vertices * sizeT times sizeR glyphs
	// prepare each buffer
	GLfloat *g_vertex_buffer_data = new GLfloat[buffersize];
	GLfloat *g_normal_buffer_data = new GLfloat[buffersize];

	unsigned int bufferi = 0;

	double delta = 0.01 * 2*M_PI / (double)s.levelOfDetail; // amount to increase alpha and beta on each iteration

	// loop through parameters & calculate
	for (int indexBeta = 0; indexBeta < sizeBeta; indexBeta++) {
		double beta = indexBeta * s.parameterStep + s.minBeta;
		for (int indexAlpha = 0; indexAlpha < sizeAlpha; indexAlpha++) {

			double alpha = indexAlpha * s.parameterStep + s.minAlpha;

		    double dT = 2.0*M_PI/(s.levelOfDetail-1);
		    double dP = M_PI/(s.levelOfDetail-1);

			double phi = -M_PI_2;

			for(int j=0;j<s.levelOfDetail;j++){

				double theta = 0;

				for(int i=0;i<s.levelOfDetail;i++){

					if (i==s.levelOfDetail-1)
						theta = 0; // this makes everything fit together!!

					glm::dvec3 p = superParameterCoord(theta,phi,alpha,beta);
					glm::dvec3 p1 = superParameterCoord(theta+delta,phi,alpha,beta);
					glm::dvec3 p2 = superParameterCoord(theta,phi+delta,alpha,beta);

					glm::dvec3 n = glm::normalize(glm::cross(p1-p, p2-p));
					capValues(n);
					//glm::vec3 n = superParameterCoord(theta,phi,2-alpha,2-beta);

					g_vertex_buffer_data[bufferi+0] = p.x; // pos
					g_vertex_buffer_data[bufferi+1] = p.y;
					g_vertex_buffer_data[bufferi+2] = p.z;

					g_normal_buffer_data[bufferi+0] = n.x; // normal
					g_normal_buffer_data[bufferi+1] = n.y;
					g_normal_buffer_data[bufferi+2] = n.z;

					theta += dT;
					bufferi+=3;
				}
				phi += dP;
			}

		}
	}

	// now write to file

	ofstream OutFile;

	string vert = s.projectData + s.preCompLocation + "Vert";
	OutFile.open(vert.c_str(), ios::out | ios::binary);
	OutFile.write( (char*)g_vertex_buffer_data, sizeof(unsigned int)*buffersize);
	OutFile.close();

	string norm = s.projectData + s.preCompLocation + "Norm";
	OutFile.open(norm.c_str(), ios::out | ios::binary);
	OutFile.write( (char*)g_vertex_buffer_data, sizeof(unsigned int)*buffersize);
	OutFile.close();

	delete [] g_vertex_buffer_data;
	delete [] g_normal_buffer_data;
}

void saveTimeStepToFile(const char* Location){
	// store all super ellipsoids to Location

	string VertexDataLocationString = Location;
	VertexDataLocationString.append("Info");
	const char* VertexDataInfoLocation = VertexDataLocationString.c_str();

	ofstream outputfile;
	outputfile.open(VertexDataInfoLocation);

	outputfile << EllipsoidDetails.size() << endl; // total number
	outputfile.close();

	outputfile.open(Location, ios::out | ios::binary);

	// for every ellipsoid
	for(unsigned int indexOfEllipse = 0 ; indexOfEllipse < EllipsoidDetails.size() ; indexOfEllipse++){

		outputfile.write( (char*)(EllipsoidDetails.at(indexOfEllipse)), sizeof(double)*13);

	}// end for


	// close file
	outputfile.close();

}

bool readTensorFileAndCreateEllipsoids(string filecode, const Matrix<double, 3, Dynamic> &TensorPositions, const Matrix<double , 1, Dynamic> &CellSizeStorage ){
	// This methods reads the specified file and creates the superEllipsoids

	string TensorLocationString = s.projectData + s.vertexDataFolder;
	string AlphaLocationString = s.projectData + s.vertexDataFolder;

	TensorLocationString = TensorLocationString + filecode + "/A2"; // complete the locations
	AlphaLocationString = AlphaLocationString + filecode + "/alpha.phase1";
	const char* TensorLocation = TensorLocationString.c_str();
	const char* AlphaLocation = AlphaLocationString.c_str();

	// Step 2.1, read Tensordata, and also the alpha channel data,
	// open Tensor and alpha files
	ifstream TensorStream;
	ifstream AlphaStream;

	TensorStream.open(TensorLocation);
	AlphaStream.open(AlphaLocation);

	if (!TensorStream.good() || !AlphaStream.good()) {
		// if the folder / file does not exist, we go to the next one
		TensorStream.close();
		AlphaStream.close();
		return false;
	}

	Matrix<Matrix3d, 1 , Dynamic> TensorStorage;
	Matrix<double , 1, Dynamic> AlphaStorage;
	string line, line2;
	int numberOfTensors;

	for(int i=0 ; i < 20 ; i++) getline(TensorStream,line); // skip 20 lines
	TensorStream >> numberOfTensors;
	if (numberOfTensors <= 0) { // quit, if we have a file in the wrong format (i.e if there is not a number in that line)
		TensorStream.close();
		AlphaStream.close();
		return false;
	}
	for(int i=0 ; i < 2 ; i++) getline(TensorStream,line); // skip 2 lines
	for(int i=0 ; i < 22 ; i++) getline(AlphaStream,line); // skip 22 lines
	//				cout << line << " alpha stream cout " << numberOfTensors <<  endl;

	// prepare the storage
	TensorStorage.resize(1,numberOfTensors);
	AlphaStorage.resize(1,numberOfTensors);

	cout << "               === read input data ... " << endl;
	// flush in all the data
	double double2 , double3 , double4, double5, double6, double7, double8;
	for(int i = 0 ; i < numberOfTensors ; i++){

		// Tensordata
		TensorStream >> line >> double2 >> double3 >> double4 >> double5 >> double6 >> double7 >> double8 >> line2; // read the line

		line.erase(0,1); // erase brackets
		line2.erase(line2.length()-1,1);

		//	cout << line << endl;
		//convert to double & save
		TensorStorage(0,i)(0,0) = atof(line.c_str());
		TensorStorage(0,i)(0,1) = double2;
		TensorStorage(0,i)(0,2) = double3;
		TensorStorage(0,i)(1,0) = double4;
		TensorStorage(0,i)(1,1) = double5;
		TensorStorage(0,i)(1,2) = double6;
		TensorStorage(0,i)(2,0) = double7;
		TensorStorage(0,i)(2,1) = double8;
		TensorStorage(0,i)(2,2) = atof(line2.c_str());

		// alphadata
		AlphaStream >> AlphaStorage(0,i); // even reads e-10 notation corretly
	}

	// close file
	TensorStream.close();
	AlphaStream.close();

	cout << "               === create Ellipsoids ... " << endl;
	// Step 2.2 Now create the Ellipsoids
	VectorXd tensorIntel;
	for(int i=0 ; i < numberOfTensors; i=i+1){ // loop through the number of Tensors

		if (clipModel) {
			if (TensorPositions(0,i) < s.clipModelMinX || TensorPositions(0,i) > s.clipModelMaxX ||
					TensorPositions(1,i) < s.clipModelMinY || TensorPositions(1,i) > s.clipModelMaxY ||
					TensorPositions(2,i) < s.clipModelMinZ || TensorPositions(2,i) > s.clipModelMaxZ)
				continue;
		}

		if (AlphaStorage(0, i) > s.GlyphAlphaThreshold) { // dont store very small glyphs
				if (CellSizeStorage(0, i) > s.CellSizeThreshold) { // dont plot tiny glyphs
					// evaluate tensor glyph details
					tensorIntel = eigenValuesToSuperEllipsoidParameters( TensorStorage(0,i), s.TensorSharpnessGammaB, s.TensorSharpnessGammaU);

					// cap to big / small alpha and beta values
					if(tensorIntel(1) > s.maxBeta) tensorIntel(1)=s.maxBeta;
					if(tensorIntel(0) > s.maxAlpha) tensorIntel(0)=s.maxAlpha;
					if(tensorIntel(1) < s.minBeta) tensorIntel(1)=s.minBeta;
					if(tensorIntel(0) < s.minAlpha) tensorIntel(0)=s.minAlpha;

					EllipsoidDetails.push_back(new double[13]);

					// store the new details
					EllipsoidDetails.back()[0]=tensorIntel(0);
					EllipsoidDetails.back()[1]=tensorIntel(1);
					EllipsoidDetails.back()[2]=TensorPositions(0,i);
					EllipsoidDetails.back()[3]=TensorPositions(1,i);
					EllipsoidDetails.back()[4]=TensorPositions(2,i);
					EllipsoidDetails.back()[5]=tensorIntel(2);
					EllipsoidDetails.back()[6]=tensorIntel(3);
					EllipsoidDetails.back()[7]=tensorIntel(4);
					EllipsoidDetails.back()[8]=tensorIntel(5);
					EllipsoidDetails.back()[9]=tensorIntel(6)*CellSizeStorage(0,i)/s.Tensorscale;
					EllipsoidDetails.back()[10]=tensorIntel(7)*CellSizeStorage(0,i)/s.Tensorscale;
					EllipsoidDetails.back()[11]=tensorIntel(8)*CellSizeStorage(0,i)/s.Tensorscale;
					EllipsoidDetails.back()[12]=AlphaStorage(0,i);
				}
		}
	}

	return true;
}

void prepareTimeSteps(){

	// STEP ZERO: determine the bounding box of those parts of the model we wish to compute / draw
	if (!(s.clipModelMaxX == 0 && s.clipModelMaxY == 0 && s.clipModelMaxZ == 0 &&
			s.clipModelMinX == 0 && s.clipModelMinY == 0 && s.clipModelMinZ == 0)) {
		clipModel = true;
		cout << " ==== computing data only for tensors between  " << endl;
		cout << "      " << s.clipModelMinX << " " << s.clipModelMinY << " " << s.clipModelMinZ << " and" << endl;
		cout << "      " << s.clipModelMaxX << " " << s.clipModelMaxY << " " << s.clipModelMaxZ << endl;

	} else {
		cout << " ==== computing data for the whole mesh  " << endl;
		clipModel=false;
	}

	// STEP ONE: read position data created by OpenFoamReader
	cout << " ==== reading position data ...  " << endl;

	//open
	ifstream positionStream;
	ifstream CellSizesStream;
	positionStream.open(s.projectData + s.tensorPositionFile);
	CellSizesStream.open(s.projectData + s.cellSizesFile);

	int numberOfPoints;
	positionStream >> numberOfPoints; // first line is number of points
	string line;
	getline(CellSizesStream, line) ; // skip one line

	// create storage for Positions
	Matrix<double, 3, Dynamic> TensorPositions;
	Matrix<double , 1, Dynamic> CellSizeStorage;
	TensorPositions.resize(3,numberOfPoints);
	CellSizeStorage.resize(1,numberOfPoints);

	// read
	for(int i=0 ; i < numberOfPoints ; i++) { // only look at existing ellipsoids
		positionStream >> TensorPositions(0,i);
		positionStream >> TensorPositions(1,i);
		positionStream >> TensorPositions(2,i);

		CellSizesStream >> CellSizeStorage(0,i);
	}

	//close Position of Tensor inputs
	positionStream.close();
	CellSizesStream.close();


	cout << " ==== now looping through files ==== " << endl;

	if (s.singlefile.length() == 0) {
		struct dirent **namelist;
		int n;

		// list the directory contents ...
		n = scandir((s.projectData + s.vertexDataFolder).c_str(), &namelist, 0, versionsort);
		if (n < 0)
			cout << "ERROR: Could not list the data directories" << endl;
		else {
			for (int i=0; i<n; i++) {
				if (namelist[i]->d_type == DT_DIR) {
					cout << "         === current fileindex: " << namelist[i]->d_name << endl;

					clearSuperEllipsoidStorage();
					cout << "         === storage cleared" << endl;

					// do the calcluations or continue if we have a file in the wrong format
					if (readTensorFileAndCreateEllipsoids(namelist[i]->d_name , TensorPositions, CellSizeStorage)) {
						// Step 2.3: save data to file
						cout << "               === save Ellipsoids ... " << endl;
						string VertexDataLocationString = s.projectData + s.vertexDataFolder;
						VertexDataLocationString = VertexDataLocationString + namelist[i]->d_name + "/VertexData";
						const char* VertexDataLocation = VertexDataLocationString.c_str();
						saveTimeStepToFile(VertexDataLocation);
					} else {
						cout << "               === skipping... " << endl;
					}
				}
				free(namelist[i]);
			}
			free(namelist);
		}
	} else {
		// calculate only one timestep - for debugging
		cout << "         === current fileindex: " << s.singlefile << endl;

		clearSuperEllipsoidStorage();
		cout << "         === storage cleared" << endl;

		// do the calcluations
		readTensorFileAndCreateEllipsoids(s.singlefile , TensorPositions, CellSizeStorage);

		// Step 2.3: save data to file
		cout << "               === save Ellipsoids ... " << endl;
		string VertexDataLocationString = s.projectData + s.vertexDataFolder;
		VertexDataLocationString = VertexDataLocationString + s.singlefile + "/VertexData";
		const char* VertexDataLocation = VertexDataLocationString.c_str();
		saveTimeStepToFile(VertexDataLocation);
	}

}
