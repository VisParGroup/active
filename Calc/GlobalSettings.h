#ifndef GLOBALSETTINGS_H_
#define GLOBALSETTINGS_H_

#include "SettingsParser.hpp"

//=======================================================================
//////////////////////////////////////////////////////////////////////////
// Global settings for ActiveCalc
//////////////////////////////////////////////////////////////////////////
//=======================================================================

struct Settings {
	std::string projectData = "/home/internshipdude/Desktop/projectdata/";
	std::string preCompLocation = "constant/PreComputedSuperEllipsoids";
	std::string vertexDataFolder = "concretePourLong/";
	std::string tensorBounds = "constant/polyMesh/meshbounds";
	std::string cellSizesFile = "constant/polyMesh/cellsizes";
	std::string tensorPositionFile = "constant/polyMesh/tensorpositions";

	std::string ownerLocation = "constant/polyMesh/owner";
	std::string pointsLocation = "constant/polyMesh/points";
	std::string facesLocation = "constant/polyMesh/faces";
	std::string neighbourLocation = "constant/polyMesh/neighbour";

	bool RunOpenFoamReader = false;
	bool PrecomputedEllipsoids = false;
	bool ComputeEllipsoidDetails = false;

	float CellSizeThreshold = 0.00001;
	float GlyphAlphaThreshold = 0.0001;
	float TensorSharpnessGammaB = 3;
	float TensorSharpnessGammaU = 3;

	std::string singlefile;

	float minAlpha=0.1; // default 0.1 http://paulbourke.net/geometry/superellipse/
	float maxAlpha=5; // default 1
	float minBeta=0.1; // default 0.1
	float maxBeta=5; // default 4
	float parameterStep = 0.05; // default 0.05

	int levelOfDetail = 11; //default 7; should be uneven
	float Tensorscale = 5; //default 5

	float clipModelMinX = 0;
	float clipModelMinY = 0;
	float clipModelMinZ = 0;
	float clipModelMaxX = 0;
	float clipModelMaxY = 0;
	float clipModelMaxZ = 0;
};

extern Settings s;

int parseSettings(const char* file);

#endif /* GLOBALSETTINGS_H_ */
