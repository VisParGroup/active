/*
 * OpenFoamReader.h
 *
 *  Created on: Sep 18, 2014
 *      Author: internshipdude
 */

#ifndef OPENFOAMREADER_H_
#define OPENFOAMREADER_H_

/* Use these methods to load all of the openFoam point/faces/owener/neigbour data into RAM */

#include <eigen3/Eigen/Dense>
using namespace Eigen;

void prepareOwnerFile();
void preparePointFile();
void prepareFaceFile();
void prepareNeighbourFile();
bool containsThisPoint(int facePointIndex, MatrixXi mat);
void calculateFinalTensorPositions();
void writeFinalTensorPositions();
void writeCellSizeData();

#endif /* OPENFOAMREADER_H_ */
