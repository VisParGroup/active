#include <iostream>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigenvalues> // to calculate eigenvalues
#include <glm/glm.hpp>

using namespace Eigen;
using namespace std;

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

#define MaxCap 100
void capValuesD (double& d) {
	if (std::isnan(d)) d = 0;
	else if (std::isinf(d)) d = MaxCap;
	else if (d > MaxCap) d = MaxCap;
}

void capValues(glm::dvec3& v) {
	capValuesD(v.x);
	capValuesD(v.y);
	capValuesD(v.z);
}

// from IEEE paper
// 0 < alpha, beta < inf; -pi <= theta <= pi; -pi/2 <= phi <= pi/2
glm::dvec3 superParameterCoord(double theta, double phi, double alpha, double beta) {
	double ct = cos(theta);
	double cp = cos(phi);
	double st = sin(theta);
	double sp = sin(phi);
	double tmp = sgn(cp)*pow(fabs(cp),beta);

	glm::dvec3 a(
			tmp * sgn(ct)*pow(fabs(ct),alpha),
			tmp * sgn(st)*pow(fabs(st),alpha),
			sgn(sp)*pow(fabs(sp),beta)
			);
	return a;
}

VectorXd eigenValuesToSuperEllipsoidParameters(Matrix3d Tensor , double sharpnessgamb, double sharpnessgamu){

	// Let's try and rotate the Tensor - WE DONT NEED THIS AFTER ALL
	//Matrix3d rotMatrix;
	//rotMatrix << 1,0,0,
	//			 0,0,-1,
	//			 0,1,0; // rotate by -90 deg around x-axis
	//Tensor = rotMatrix * Tensor * rotMatrix.transpose(); // Tensor rotation

	// TRANSFORM THE MATRIX INTO A TRACELESS TENSOR
	// page 1198
	Tensor = Tensor - 1.0/3.0*MatrixXd::Identity(3,3);

	EigenSolver<Matrix3d> solver( Tensor ); // compute EVals and EVec

	//cout << solver.eigenvalues() << endl;
	double eig1 = solver.eigenvalues()[0].real();//to get the relevant value, the imaginary part should be zero anyway
	double eig2 = solver.eigenvalues()[1].real();
	double eig3 = solver.eigenvalues()[2].real();

	int eig1vec=0; // important when swapping
	int eig2vec=1;
	int eig3vec=2;

	// SORT BY MAGNITUDE
	if (fabs(eig1) > fabs(eig2) ) {
		if (fabs(eig3) > fabs(eig1) ) {	swap(eig1,eig3);	swap(eig1vec,eig3vec);	}
	} else {
		if (fabs(eig2) > fabs(eig3) ) { swap(eig1,eig2); swap(eig1vec,eig2vec); }
		else { swap(eig1,eig3); swap(eig1vec,eig3vec); }
	}
	if(fabs(eig3) > fabs(eig2) ) { swap(eig2,eig3); swap(eig2vec,eig3vec); }

	// TRY USING THE IEEE Paper computations
	// Page 1199, equations 4-7
	double S = 1.5*eig1;
	double muuplus, muuminus, mub, mui;
	if(S>=0){
		muuplus=-3*eig3;
		muuminus=0;
		mui = 1- 1.5*eig1;
	} else {
		muuplus=0;
		muuminus=6*eig3;
		mui = 1+ 3*eig1;
	}
	mub=fabs(3*eig1+6*eig3);

	// equation 8
	double alpha=0, beta=0;

	if(muuplus>=0 && muuminus == 0){
		alpha = pow(1-mub,sharpnessgamb);
		beta = pow(1-muuplus,sharpnessgamu);

	}
	if (muuplus==0 && muuminus > 0) {
		alpha = pow(1-mub,sharpnessgamb);
		beta = 1+3*pow(muuminus,sharpnessgamu);
	}

	// page 1200
	// Evaluate stretch factors
	double strechX=1, strechY=1, strechZ=1;

	// define range of strechfactors
	double strechMax=0.5; // in paper: 0.5, marcel: 1.0
	double strechMin=0.1;

	// equations 9 and 10
	if (muuplus >= muuminus){
		strechX = strechMin + (strechMax-strechMin)*mui;
		strechY = strechMin + (strechMax-strechMin)*(1-muuplus);
	} else {
		strechX = strechMin + (strechMax-strechMin)*(1-0.5*mub);
		strechY=strechMax;
	}
	strechZ=strechMax;


	// according to the source, we must now rotate to eigenvector frame
	// access the columns
	Vector3d eigVector1 = solver.eigenvectors().col(eig1vec).real();
	Vector3d eigVector2 = solver.eigenvectors().col(eig2vec).real();
	Vector3d eigVector3 = solver.eigenvectors().col(eig3vec).real();
	eigVector1.normalize();
	eigVector2.normalize();
	eigVector3.normalize();


	// Marcel's solution:
//	double rotationAngle1 = 180/M_PI*acos(eigVector1.dot(Vector3d::UnitZ()));
//	Vector3d rotationAxis1 =  eigVector1.cross(Vector3d::UnitZ());
//	rotationAxis1 = rotationAngle1*rotationAxis1.normalized();
//	if(rotationAngle1<0) rotationAxis1 = -rotationAxis1;

	// Next try
	// should be Z, Y is a hack to align it properly right now
//	double rotationAngle = 180/M_PI*acos(eigVector1.dot(Vector3d::UnitY()));
//	Vector3d rotationAxis =  eigVector1.cross(Vector3d::UnitY());
//	rotationAxis = rotationAxis.normalized();
//
//	Quaterniond quat(
//			cos (rotationAngle/2),
//			rotationAxis(0) * sin(rotationAngle/2.0),
//			rotationAxis(1) * sin(rotationAngle/2.0),
//			rotationAxis(2) * sin(rotationAngle/2.0)
//			);
//	quat.normalize();


	// With change of base matrix http://www.mathworks.com/matlabcentral/newsreader/view_thread/156864
	Matrix3d rot;
	rot <<  eigVector3.x(), eigVector2.x(), eigVector1.x(),
			eigVector3.y(), eigVector2.y(), eigVector1.y(),
			eigVector3.z(), eigVector2.z(), eigVector1.z();

	Quaterniond quat;
	quat=rot;

	//	return the information in the following format:
	//  paramT, ParamT,  xrot, yrot, zrot, wrot, xscale,yscale,zscale,, note: THE POSITION COME FROM THE TENSOR DATA
	VectorXd data;
	data.resize(9);
	data(0)=alpha;
	data(1)=beta;
	data(2)=quat.x();
	data(3)=quat.y();
	data(4)=quat.z();
	data(5)=quat.w();
	data(6)=strechX;
	data(7)=strechY; // Z
	data(8)=strechZ; // Y
	return data;
}
