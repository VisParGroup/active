//============================================================================
// Name        : ActiveCalc.cpp
// Author      : 
// Version     :
// Copyright   : 
// Description : Hello World in C, Ansi-style
//============================================================================


#include <iostream>

#include "OpenFoamReader.h"
#include "Tensors.h"
#include "GlobalSettings.h"

using namespace std;

int main(void) {
	clock_t beginCreate;
	clock_t endCreate;

	parseSettings("../ActiveSettings.conf");

	if (s.RunOpenFoamReader) {
	beginCreate = clock();
	cout << "  ===== STARTING OpenFoam Reader ======  " << endl;
	cout << "  ===== STARTING READING ======  " << endl;
	// start reading ownerfile just to get the sizes, has to be done first
	prepareOwnerFile();
	preparePointFile();
	prepareFaceFile();
	prepareNeighbourFile();

	// note: we want to use this information in order to find the positions of the tensors
	cout << "  ===== EVALUTE POSITIONS ======  " << endl;
	calculateFinalTensorPositions();
	//	cout << " first cell average x is = " << FOAM_FinalTensorPositionStorage(0,0) << endl;

	cout << "  ===== WRITE POSITIONS ======  " << endl;
	writeFinalTensorPositions();

	cout << "  ===== WRITE CELL SIZES======  " << endl;
	writeCellSizeData();

	// in the final step we will output all positions and Tensors into a new file
	endCreate = clock();
	cout << "  ===== OpenFoam Reader finished ======  " << (double(endCreate - beginCreate)/60/ CLOCKS_PER_SEC) << " mins "<< endl;
	}

	if (s.PrecomputedEllipsoids) {
	beginCreate = clock();
	cout << " ===== generating precomputed super ellipsoids ==== " << endl;
	precomputeSuperEllipsoids();
	endCreate = clock();
	cout << " ===== done generating precomputed super ellipsoids ==== " << (double(endCreate - beginCreate)/60/ CLOCKS_PER_SEC) << " mins "<< endl;
	}

	if (s.ComputeEllipsoidDetails) {
	beginCreate = clock();
	cout << " ===== generating super ellipsoid details ==== " << endl;
	prepareTimeSteps();
	endCreate = clock();
	cout << " ===== done generating super ellipsoid details ==== " << (double(endCreate - beginCreate)/60/ CLOCKS_PER_SEC) << " mins "<< endl;
	}
}
