![active2.jpg](https://bitbucket.org/repo/e9oAea/images/2643365596-active2.jpg)

# README for A.C.T.I.V.E
-------------------------

## Overview

Active is a scalable superellipsoid-based CFD visualization for virtual and desktop environments. Read the paper ;) (http://diglib.eg.org/handle/10.2312/eurovr.20141344.089-092)  
It is divided into two sub-programs: ActiveCalc takes a bunch of OpenFOAM simulation data and computes superellipsoid parameters for each time step as well as precomputes superellipsoid shapes for the visualization.  
The visualization is done in the ActiveSim program, which reads the files created by Calc and displays the ellipsoids in a VRUI environment with animation and the ability to influence some visualization parameters.  
There is also the "GlyphDemo" program that displays only a single superellipsoid according to paramters chosen by the user.


## Requirements

In order for the two programs to compile, some prerequisites must be met:

* The compiler must support C++11 (although this is only a requirement for the Config file parser, which can easily removed from the code, so that it compiles with any C++ standard)
* VRUI must be installed and configured with the additional instanced arrays extension! (see the VRUI extension folder, was tested with Vrui-3.1-003)
* OpenGL (tested with version 4.2), GL mathematics and Eigen libraries must be installed
* The graphics card / driver must support vertex and fragment shaders and the GL_ARB_draw_instanced and GL_ARB_instanced_arrays extensions

on debian run (as root)
```
apt-get install build-essential
apt-get install zlib1g-dev mesa-common-dev libgl1-mesa-dev libglu1-mesa-dev
apt-get install libglm-dev libeigen3-dev
```

## Build and run ActiveCalc

    cd Calc
    make clean
    make     #(or make DEBUG=1)
    ./bin/ActiveCalc	#(or ./bin/debug/ActiveCalc)

*be sure to be in the correct directory when starting ActiveCalc, the path for the config is currently relative to the directory* (we know it's a stupid issue)

## Build and run ActiveSim

    cd Sim

Now, open ./Makefile and make sure that VRUI_MAKEDIR points to the correct folder where Vrui was installed (usually $(HOME)/Vrui-3.1/share/). Then:

    make clean
    make    #(or make DEBUG=1)
    ./bin/Active	#(or ./bin/debug/Active)

*be sure to be in the correct directory when starting Active, the path for the config is currently relative to the directory* (we know it's a stupid issue)


## Build and run GlyphDemo

    cd GlyphDemo

Now, open ./Makefile and make sure that VRUI_MAKEDIR points to the correct folder where Vrui was installed (usually $(HOME)/Vrui-3.1/share/). Then:

    make clean
    make     #(or make DEBUG=1)
    ./bin/GlyphDemo	#(or ./bin/debug/GlyphDemo)

The Tensor shown by the "Toggle custom tensor" option comes from the "tensor" file inside the GlyphDemo directory.


## Configuring Active

In the parent folder of ActiveSim and ActiveCalc alongside this README there is a file called ActiveSettings.conf. The options here apply to both programs the most important setting is the first, because "projectData" must point to the correct folder. All the other options are explained in the config.

## Citation

If you use A.C.T.I.V.E. for your research, please cite:

Heiko Herrmann, Marcel Padilla, and Emiliano Pastorelli. A.C.T.I.V.E.: *A Scalable Superellipsoid-based CFD Visualization for Virtual and Desktop Environments*. In Jerome Perret, Valter Basso, Francesco Ferrise, Kaj Helin, Vincent Lepetit, James Ritchie, Christoph Runde, Mascha van der Voort, and Gabriel Zachmann, editors, EuroVR 2014 - Conference and Exhibition of the European Association of Virtual and Augmented Reality. The Eurographics Association, December 2014. DOI: [10.2312/eurovr.20141344](http://dx.doi.org/10.2312/eurovr.20141344)

## Disclaimer

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.